      MODULE globalmod_constants
          implicit none
          integer*4, parameter           ::  idread = 5, idwrite = 6
          character(LEN=50)              ::  machine_name
          integer*4                      ::  version,subversion
          integer, parameter             ::  idp = 8,master_id = 0
          integer*4                      ::  my_id,numprocs
      END MODULE globalmod_constants



!=================================================================!
! Find all prime integers below N.                                !
!-----------------------------------------------------------------!
! (1) assume pmin = 2.                                            !
! (2) undefine the array for integer list <= N.                   !
! (3) only determine the first multiple of k in local region.     !
! (4) all the rest multiple of k move with the step of k.         !
! (5) delete all even integers, except 2.                         !
!-----------------------------------------------------------------!
! Xiaoxu Guan at LSU HPC                                          !
! April 18, 2016                                                  !
!=================================================================!
      program mpi_primes_v3
      use globalmod_constants
      implicit none
      include 'mpif.h'
      integer                 :: ierr,my_len
      integer*4               :: pmin,pmax,istart,iend,itemp,i,k,kmin,istart_min,iend_max,pcount,ptot,  &
     &                           pmax_org,lk,llk
      real(KIND=idp)          :: time_s,time_e,what_time,temp
      character(LEN=20)       :: message
      integer*4               :: istatus(MPI_STATUS_SIZE)
      integer                 :: argc            ! Number of command line arguments.
      integer                 :: iargc           ! External function returning argc.
      character(LEN=80)       :: arg             ! Container for command line arguments.
      integer*4               :: chunk_floor,chunk_ceili,chunk,rmn
!!      integer*4, allocatable, dimension(:)  :: idata     ! a list of !integers.
      logical, allocatable, dimension(:)    :: marked




      call MPI_INIT( ierr )
      call MPI_COMM_SIZE( MPI_COMM_WORLD, numprocs, ierr )
      call MPI_COMM_RANK( MPI_COMM_WORLD, my_id, ierr )

      call MPI_GET_PROCESSOR_NAME( machine_name, my_len, ierr )
      call MPI_GET_VERSION( version, subversion, ierr )


      if(my_id == master_id ) then
         message = 'Job submitted on :: '
         call datetime(message)
         time_s = MPI_WTIME()
      end if

      call MPI_BARRIER( MPI_COMM_WORLD,ierr )

      argc=iargc()
      if (argc /= 2) then ! number of input arguments must be 2.

          if (my_id == master_id) then
              write(idwrite,'(1x,a,i3)') 'Number of input arguments is ', argc
              write(idwrite,'(1x,a)')    'It must be 2.'
              write(idwrite,'(1x,a)')    'Quit!'
          end if
              call MPI_FINALIZE(ierr)
              stop
       else    ! passing the arguments.

          call getarg(1, arg)
          read(arg, '(i)') pmin   ! left limit.
          call getarg(2, arg)
          read(arg, '(i)') pmax   ! right limit.

          if(my_id == master_id) then
             write(idwrite,'(1x,a,i10)')  'Lower limit: ', pmin
             write(idwrite,'(1x,a,i10)')  'Upper limit: ', pmax
             write(idwrite,*)
          end if
       end if

      call MPI_BARRIER( MPI_COMM_WORLD,ierr )

! domain decomposition.
      pmax_org = pmax

      if( mod(pmax,2) == 0 ) pmax = pmax - 1 
      lk = (pmax-1)/2    ! lk is the total number of all odd integers to search through.


      temp = dble(lk) / dble(numprocs)
      chunk_floor = floor(temp) 
      chunk_ceili = ceiling(temp) 

      rmn = mod(lk,numprocs) 

! istart and iend are all odd integers that need to be searched through.
      if( rmn > 0 .and. my_id <= rmn-1 ) then
          chunk  = chunk_ceili
          istart = my_id * chunk * 2 + 1 + 2
          iend   = istart + (chunk - 1) * 2
      elseif( rmn > 0 .and. my_id > rmn-1 ) then
          chunk  = chunk_floor
          itemp  = chunk_ceili * rmn 
          itemp  = itemp * 2 + 1
          istart = itemp + 2 + (my_id - rmn) * chunk * 2
          iend   = istart + (chunk - 1) * 2
      elseif(rmn == 0) then
          chunk = chunk_floor
          istart = my_id * chunk * 2 + 1 + 2
          iend   = istart + (chunk - 1) * 2
      end if


      ALLOCATE( marked(1:chunk) )
      marked  = .true.


      write(idwrite,'(1x,i3,2(a,i10),a,i10,2(a,i10))') my_id,      &
     & '  istart = ', istart,'  iend = ',   &
     & iend, '  size = ', chunk,'  step = 2'
      write(idwrite,*)


      call MPI_BARRIER( MPI_COMM_WORLD,ierr )


! start the Sieve of Eratosthenes searching.
         iend_max   = min(iend,pmax)
         k = 3

      do while( k*k <= pmax )
         istart_min = max(istart,k*k)
! determine the first multiple of k.
         rmn = mod(istart_min,k)
         if(rmn /= 0) istart_min = istart_min - rmn + k  

      do i = istart_min, iend_max, k
!!         itemp = mod(idata(i),k) 
!         itemp = mod(i+2,k) 
!         if(itemp == 0) marked(i) = .false.

         if( mod(i,2) /= 0 ) then
             lk = (i - istart) / 2 + 1
             marked(lk) = .false.
          end if
      end do

! makred(i) = true means unmarked, while = false means marked.
! find out the smaller unmarked array element > k.

         kmin = pmax   ! avoid infinite loops.
      smallest_k:  do i = istart, iend, 2
         llk = (i - istart) / 2 + 1
         if( marked(llk) .and. i > k ) then 
             kmin = i
             EXIT smallest_k
         end if
      end do  smallest_k



      call MPI_ALLREDUCE(kmin,k,1,MPI_INTEGER,MPI_MIN,MPI_COMM_WORLD,ierr)

      end do


            pcount = 0
      do i = istart, iend, 2
         lk = (i - istart) / 2 + 1
         if( marked(lk) ) then
            pcount = pcount + 1
         end if
      end do


      call MPI_REDUCE(pcount,ptot,1,MPI_INTEGER,MPI_SUM,master_id,MPI_COMM_WORLD,ierr)
      ptot = ptot + 1

      if(my_id == master_id) then 
         time_e = MPI_WTIME()
         write(idwrite,'(1x,a,i10,a,i10)')     &
     &   'No of primes below ',pmax_org, ' is ', ptot
          write(idwrite,'(1x,a,f10.4)') 'Elapsed time (sec) =  ', time_e - time_s
      end if


      DEALLOCATE( marked )

      call MPI_BARRIER( MPI_COMM_WORLD,ierr )




      if(my_id == master_id ) then
         write(idwrite,*)
         write(idwrite,*)
         message = 'Job finished on  :: '
         call datetime(message)
      end if


      call MPI_FINALIZE(ierr)

      end program mpi_primes_v3



!===========================================================!
! This subroutine reads the input parameters and generate   !
! the mesh points of xi, eta, and other global variables.   !
! 'ier_read' returns the error code.                        !
!  ier_read = 0 for normal return without errors.           !
! Otherwise, it takes a non-zero integer.                   !
!===========================================================!
      subroutine datetime(message)
      use globalmod_constants, only  : idwrite
      implicit none
      character(LEN=20), intent(in) :: message
      character(LEN=8)              :: today
      character(LEN=10)             :: now


      call date_and_time (today, now)
! today: mm/dd/yyyy
      write(idwrite,'(1x,a,a2,a,a2,a,a4,a)') message, today(5:6),        &
                                     '/', today(7:8), '/', today(1:4),   &
                                     '  (/mm/dd/yyyy)'
! now: hhmmss.sss
      write(idwrite,'(1x,a,13x,a,a2,a,a2,a,a5)') 'Time',':: ',now(1:2), ':',    &
                                                           now(3:4), ':', now(5:10)
      write(idwrite,*)

      return
      end subroutine datetime

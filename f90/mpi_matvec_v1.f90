      MODULE globalmod_constants
          implicit none
          integer*4, parameter           ::  idread = 5, idwrite = 6
          character(LEN=50)              ::  machine_name
          integer*4                      ::  version,subversion
          integer, parameter             ::  idp = 8,master_id = 0
          integer*4                      ::  my_id,numprocs

          integer*4                      ::  nsize,chunk,istart,iend
          integer*4, allocatable, dimension(:)   :: pcount_vec_all,vector_index
          integer*4, allocatable, dimension(:)   :: rdispl_all,rcounts,sdispl_all,scounts
      END MODULE globalmod_constants




!=================================================================!
! Matrix-vector products c = A * b.                               !
!-----------------------------------------------------------------!
! (1) square dense martrix.                                       !
! (2) columnwise block decomposition for matrix.                  !
! (3) block decomposed for both vectors b and c.                  !
! (4) MPI_ALLTOALLV for vector communication.                     !
! (5) unit stride for local matrix-vector products.               !
!-----------------------------------------------------------------!
! Xiaoxu Guan at LSU HPC                                          !
! April 18, 2016                                                  !
!=================================================================!
      program mpi_matvec_v1
      use globalmod_constants
      implicit none
      include 'mpif.h'
      integer                 :: ierr,my_len
      integer*4               :: itemp,i,j,k,nmt,istart_min,iend_max,pcount,ptot,  &
     &                           lk,llk
      real(KIND=idp)          :: time_s,time_e,what_time,temp,elapsed_time
      character(LEN=20)       :: message
      integer*4               :: istatus(MPI_STATUS_SIZE)
      integer                 :: argc            ! Number of command line arguments.
      integer                 :: iargc           ! External function returning argc.
      character(LEN=80)       :: arg             ! Container for command line arguments.
      integer*4               :: chunk_floor,chunk_ceili,rmn,pcount_mat,pcount_vec,km,kv,psize_mat,psize_vec
      integer*4, allocatable, dimension(:)         :: pcount_all,pcount_mat_all
      real(KIND=idp), allocatable, dimension(:,:)  :: matrix
      real(KIND=idp), allocatable, dimension(:)    :: matrix_data_inp,vector,vector_data_inp,&
                                                      vector_inp,vector_out

! MPI I/O:
      character(LEN=19), parameter   ::                           &
                         matrix_filename = 'matrix_elements.dat', &
                         vector_filename = 'vector_elements.dat'

      integer*4                      :: filename_mat,filename_vec,irequest,icount,ifs,psize,file_size,iextent_real8,ifilesize,jrequest
      integer(KIND=MPI_OFFSET_KIND)  :: ioffset_mat,ioffset_vec,ioffset_view





      call MPI_INIT( ierr )
      call MPI_COMM_SIZE( MPI_COMM_WORLD, numprocs, ierr )
      call MPI_COMM_RANK( MPI_COMM_WORLD, my_id, ierr )

      call MPI_GET_PROCESSOR_NAME( machine_name, my_len, ierr )
      call MPI_GET_VERSION( version, subversion, ierr )


      if(my_id == master_id ) then
         message = 'Job submitted on :: '
         call datetime(message)
         time_s = MPI_WTIME()
      end if

      call MPI_BARRIER( MPI_COMM_WORLD,ierr )

      argc=iargc()
      if (argc /= 1) then ! number of input arguments must be 1.

          if (my_id == master_id) then
              write(idwrite,'(1x,a,i3)') 'Number of input arguments is ', argc
              write(idwrite,'(1x,a)')    'It must be 1.'
              write(idwrite,'(1x,a)')    'Quit!'
          end if
              call MPI_FINALIZE(ierr)
              stop
       else    ! passing the arguments.

          call getarg(1, arg)
          read(arg, '(i)') nsize   ! matrix size.

          if(my_id == master_id) then
             write(idwrite,'(1x,a,i10)')  'Matrix size: ', nsize
             write(idwrite,*)
          end if
       end if

      call MPI_BARRIER( MPI_COMM_WORLD,ierr )

! domain decomposition (columnwise blocks).
      temp = dble(nsize) / dble(numprocs)
      chunk_floor = floor(temp) 
      chunk_ceili = ceiling(temp) 

      rmn = mod(nsize,numprocs) 

! note global indices istart and iend are the array indices starting from 0 for the first core.
! the indices i in array corresponds to integer i+2.
      if( rmn > 0 .and. my_id <= rmn-1 ) then
          chunk  = chunk_ceili
          istart = my_id * chunk + 1
          iend   = istart + chunk - 1
      elseif( rmn > 0 .and. my_id > rmn-1 ) then
          chunk  = chunk_floor
          itemp  = chunk_ceili * rmn 
          istart = itemp + 1 + (my_id - rmn ) * chunk
          iend   = istart + chunk - 1
      elseif(rmn == 0) then
          chunk = chunk_floor
          istart = my_id * chunk + 1
          iend   = istart + chunk - 1
      end if

      write(idwrite,'(1x,4(i4,3x))')  my_id, istart,iend, chunk


! MPI Input.

      ALLOCATE( matrix(1:nsize,istart:iend), vector(istart:iend)  )
      ALLOCATE( pcount_mat_all(0:numprocs-1), pcount_vec_all(0:numprocs-1) )

      if(my_id == master_id) then
            pcount_mat_all(0) = chunk*nsize + 5
            psize_mat = pcount_mat_all(0)
            pcount_vec_all(0) = chunk + 5
            pcount_vec = pcount_vec_all(0)
            psize_vec = pcount_vec
         do i = 1, numprocs-1
            call MPI_RECV(k,1,MPI_INTEGER,i,0,MPI_COMM_WORLD,istatus,ierr)
            pcount_mat_all(i) = k
            call MPI_RECV(j,1,MPI_INTEGER,i,1,MPI_COMM_WORLD,istatus,ierr)
            pcount_vec_all(i) = j
         end do

      else
            pcount_mat = chunk*nsize + 5
            psize_mat = pcount_mat
            pcount_vec = chunk + 5
            psize_vec = pcount_vec
            call MPI_SEND(pcount_mat,1,MPI_INTEGER,master_id,0,MPI_COMM_WORLD,ierr)
            call MPI_SEND(pcount_vec,1,MPI_INTEGER,master_id,1,MPI_COMM_WORLD,ierr)
      end if


      call MPI_BCAST(pcount_mat_all,numprocs,MPI_INTEGER,master_id,MPI_COMM_WORLD,ierr)
      call MPI_BCAST(pcount_vec_all,numprocs,MPI_INTEGER,master_id,MPI_COMM_WORLD,ierr)

!!         if(my_id==0)  write(*,*) 'mat_vec = ',pcount_vec_all

      nmt = nsize * chunk + 5
      ALLOCATE( matrix_data_inp(1:nmt), vector_data_inp(1:chunk+5) )


! get offset.
      if(my_id == master_id) then
         ioffset_mat = 0
         ioffset_vec = 0
      else
         ioffset_mat = SUM( pcount_mat_all(0:my_id-1) )
         ioffset_vec = SUM( pcount_vec_all(0:my_id-1) )
      end if

      call MPI_FILE_OPEN(MPI_COMM_WORLD,matrix_filename,    &
                         MPI_MODE_RDONLY,MPI_INFO_NULL,filename_mat,ierr)

      call MPI_FILE_OPEN(MPI_COMM_WORLD,vector_filename,    &
                         MPI_MODE_RDONLY,MPI_INFO_NULL,filename_vec,ierr)


      ioffset_view = 0
      call MPI_FILE_SET_VIEW(filename_mat,ioffset_view,MPI_REAL8,MPI_REAL8,  &
                             'native',MPI_INFO_NULL,ierr)
 
      call MPI_FILE_SET_VIEW(filename_vec,ioffset_view,MPI_REAL8,MPI_REAL8,  &
                             'native',MPI_INFO_NULL,ierr)

! each MPI task reads its own part to the same file in binary format.
      call MPI_FILE_READ_AT(filename_mat,ioffset_mat,matrix_data_inp,psize_mat,MPI_REAL8,istatus,ierr)
      call MPI_FILE_READ_AT(filename_vec,ioffset_vec,vector_data_inp,psize_vec,MPI_REAL8,istatus,ierr)


      call MPI_BARRIER( MPI_COMM_WORLD,ierr )


! check the matrix parameters.
      if( numprocs /= INT(matrix_data_inp(1)) ) then
          write(idwrite,'(1x,a,i4)') 'my_id = ',my_id
          write(idwrite,'(1x,a)') 'numprocs /= matrix_data_inp(1) !'
          write(idwrite,'(1x,a,i4)') 'numprocs = ', numprocs
          write(idwrite,'(1x,a,i4)') 'INT(matrix_data_inp(1)) = ', INT(matrix_data_inp(1))
          write(idwrite,'(1x,a)')  'Quit!'
          call MPI_FINALIZE(ierr)
          stop
      end if


      if( nsize /= INT(matrix_data_inp(2)) ) then
          write(idwrite,'(1x,a,i4)') 'my_id = ',my_id
          write(idwrite,'(1x,a)') 'nsize /= INT(matrix_data_inp(2) !'
          write(idwrite,'(1x,a,i4)') 'nsize = ', nsize
          write(idwrite,'(1x,a,i4)') 'INT(matrix_data_inp(2)) = ', INT(matrix_data_inp(2))
          write(idwrite,'(1x,a)')  'Quit!'
          call MPI_FINALIZE(ierr)
          stop
      end if

      if( chunk /= INT(matrix_data_inp(3)) ) then
          write(idwrite,'(1x,a,i4)') 'my_id = ',my_id
          write(idwrite,'(1x,a)') 'chunk /= INT(matrix_data_inp(3) !'
          write(idwrite,'(1x,a,i4)') 'chunk = ', chunk
          write(idwrite,'(1x,a,i4)') 'INT(matrix_data_inp(3)) = ', INT(matrix_data_inp(3))
          write(idwrite,'(1x,a)')  'Quit!'
          call MPI_FINALIZE(ierr)
          stop
      end if


      if( istart /= INT(matrix_data_inp(4)) ) then
          write(idwrite,'(1x,a,i4)') 'my_id = ',my_id
          write(idwrite,'(1x,a)') 'istart /= INT(matrix_data_inp(4) !'
          write(idwrite,'(1x,a,i4)') 'istart = ', istart
          write(idwrite,'(1x,a,i4)') 'INT(matrix_data_inp(4)) = ', INT(matrix_data_inp(4))
          write(idwrite,'(1x,a)')  'Quit!'
          call MPI_FINALIZE(ierr)
          stop
      end if

      if( iend /= INT(matrix_data_inp(5)) ) then
          write(idwrite,'(1x,a,i4)') 'my_id = ',my_id
          write(idwrite,'(1x,a)') 'iend /= INT(matrix_data_inp(5) !'
          write(idwrite,'(1x,a,i4)') 'iend = ', iend
          write(idwrite,'(1x,a,i4)') 'INT(matrix_data_inp(5)) = ', INT(matrix_data_inp(5))
          write(idwrite,'(1x,a)')  'Quit!'
          call MPI_FINALIZE(ierr)
          stop
      end if


! check the vector parameters.
      if( numprocs /= INT(vector_data_inp(1)) ) then
          write(idwrite,'(1x,a,i4)') 'my_id = ',my_id
          write(idwrite,'(1x,a)') 'numprocs /= vector_data_inp(1) !'
          write(idwrite,'(1x,a,i4)') 'numprocs = ', numprocs
          write(idwrite,'(1x,a,i4)') 'INT(vector_data_inp(1)) = ', INT(vector_data_inp(1))
          write(idwrite,'(1x,a)')  'Quit!'
          call MPI_FINALIZE(ierr)
          stop
      end if

      if( nsize /= INT(vector_data_inp(2)) ) then
          write(idwrite,'(1x,a,i4)') 'my_id = ',my_id
          write(idwrite,'(1x,a)') 'nsize /= vector_data_inp(2) !'
          write(idwrite,'(1x,a,i4)') 'nsize = ', nsize
          write(idwrite,'(1x,a,i4)') 'INT(vector_data_inp(2)) = ', INT(vector_data_inp(2))
          write(idwrite,'(1x,a)')  'Quit!'
          call MPI_FINALIZE(ierr)
          stop
      end if

      if( chunk /= INT(vector_data_inp(3)) ) then
          write(idwrite,'(1x,a,i4)') 'my_id = ',my_id
          write(idwrite,'(1x,a)') 'chunk /= vector_data_inp(3) !'
          write(idwrite,'(1x,a,i4)') 'chunk = ', chunk
          write(idwrite,'(1x,a,i4)') 'INT(vector_data_inp(3)) = ', INT(vector_data_inp(3))
          write(idwrite,'(1x,a)')  'Quit!'
          call MPI_FINALIZE(ierr)
          stop
      end if

      if( istart /= INT(vector_data_inp(4)) ) then
          write(idwrite,'(1x,a,i4)') 'my_id = ',my_id
          write(idwrite,'(1x,a)') 'istart /= vector_data_inp(4) !'
          write(idwrite,'(1x,a,i4)') 'istart = ', istart
          write(idwrite,'(1x,a,i4)') 'INT(vector_data_inp(4)) = ', INT(vector_data_inp(4))
          write(idwrite,'(1x,a)')  'Quit!'
          call MPI_FINALIZE(ierr)
          stop
      end if

      if( iend /= INT(vector_data_inp(5)) ) then
          write(idwrite,'(1x,a,i4)') 'my_id = ',my_id
          write(idwrite,'(1x,a)') 'iend /= vector_data_inp(5) !'
          write(idwrite,'(1x,a,i4)') 'iend = ', iend
          write(idwrite,'(1x,a,i4)') 'INT(vector_data_inp(5)) = ', INT(vector_data_inp(5))
          write(idwrite,'(1x,a)')  'Quit!'
          call MPI_FINALIZE(ierr)
          stop
      end if


! retrieve matrix and vector from the input data.
         km = 5
         kv = 5
      do j = istart, iend
         kv = kv + 1
         vector(j) = vector_data_inp(kv)
      do i = 1, nsize
         km = km + 1
         matrix(i,j) = matrix_data_inp(km)
      end do
      end do

      call MPI_BARRIER( MPI_COMM_WORLD,ierr )

! test matrix and vector elments.
      do j = istart, iend   ! columm
         if( ABS(vector(j) - COS( dble(j)) ) >= 1.d-10 ) &
             write(*,*) 'Somethig wrong with vector elements!'
      do i = 1, nsize       ! row
         if(i <= j) then 
            temp = SIN( dble(i-j) ) **(i+j)   ! matrix elements.
         else
            temp = COS( dble(i-j) ) **(i+j)   ! matrix elements.
         end if

         if( ABS(matrix(i,j) - temp) >= 1.d-10 )  &
             write(*,*) 'Somethig wrong with matrix elements!'
      end do
      end do

! only master does all things.
      if(my_id==master_id) then
      do i = 1, nsize
         temp = 0.0_idp
      do j = 1, nsize
         temp =  temp + SIN( dble(i-j) ) * COS( dble(j) )
      end do
!!         write(9,'(1x,i6,2x,es22.15)')  i,temp
      end do
      end if
! end of master block.



! define arrays for data communication in matrix-vector products.
      pcount_vec_all = pcount_vec_all - 5

!!      ALLOCATE ( vector_index(0:numprocs-1) )
!!         vector_index(0) = 0
!!      do i = 1, numprocs-1
!!         vector_index(i) = SUM( pcount_vec_all(0:i-1) )
!!      end do


! define arrays for matrix-vector products.
      ALLOCATE( rdispl_all(0:numprocs-1),  &
                rcounts(0:numprocs-1),     &
                sdispl_all(0:numprocs-1),  &
                scounts(0:numprocs-1) )

         scounts(0) = pcount_vec_all(0)
         sdispl_all(0) = 0
      do i = 1, numprocs-1
         sdispl_all(i) = sdispl_all(i-1) + scounts(i-1)
         scounts(i) = pcount_vec_all(i)
      end do
   
         rcounts(0) = pcount_vec_all(my_id)
         rdispl_all(0) = 0
      do i = 1, numprocs-1
         rcounts(i) = pcount_vec_all(my_id)
         rdispl_all(i) = rdispl_all(i-1) + rcounts(i-1)
      end do

! all matrix and vector elemets are ready at this point.
      ALLOCATE( vector_inp(1:chunk),vector_out(1:chunk) )
      vector_inp = vector

      if(my_id == master_id ) then
         time_s = MPI_WTIME()
      end if

!!      do k = 1, 10000
      do k = 1, 10000
         call matvec(matrix,vector_inp,vector_out)
         vector_inp = vector_out
      end do

      if(my_id == master_id ) then
         elapsed_time = MPI_WTIME() - time_s
         write(idwrite,*)
         write(idwrite,'(1x,a,f12.6)') 'Elapsed_time (sec) = ', elapsed_time
      end if


      DEALLOCATE( matrix, vector )
      DEALLOCATE( pcount_mat_all, pcount_vec_all )
      DEALLOCATE( matrix_data_inp, vector_data_inp )
      DEALLOCATE( rdispl_all, rcounts, sdispl_all, scounts )
      DEALLOCATE( vector_inp )


      if(my_id == master_id ) then
         write(idwrite,*)
         write(idwrite,*)
         message = 'Job finished on  :: '
         call datetime(message)
      end if


      call MPI_FINALIZE(ierr)

      end program mpi_matvec_v1



!===========================================================!
! MPI version of matrix-vector product. c = A * b.          ! 
!===========================================================!
      subroutine matvec(matrix,vector_inp,vector_out)
      use globalmod_constants, only  : idp,my_id,numprocs,nsize,chunk,istart,iend,  &
                                       vector_index,pcount_vec_all,scounts,rcounts, &
                                       sdispl_all,rdispl_all
      implicit none
      include 'mpif.h'
      real(kind=idp), dimension(1:nsize,istart:iend), intent(in)   :: matrix
      real(kind=idp), dimension(istart:iend), intent(in)   :: vector_inp
      real(kind=idp), dimension(istart:iend), intent(out)  :: vector_out
      real(kind=idp), dimension(1:nsize)                   :: c_local_temp
      real(kind=idp), dimension(1:chunk*numprocs)          :: c_local_out
      integer*4        :: i,j,k,ierr
      real(kind=idp)   :: temp
     

         c_local_temp = 0.0_idp
      do j = istart, iend
      do i = 1, nsize
         c_local_temp(i) = c_local_temp(i) + matrix(i,j) * vector_inp(j)
      end do
      end do

!!      write(10+my_id,'(es22.15)') c_local_temp

!!         scounts(0) = pcount_vec_all(0)
!!         sdispl_all(0) = 0
!!      do i = 1, numprocs-1
!!         sdispl_all(i) = sdispl_all(i-1) + scounts(i-1)
!!         scounts(i) = pcount_vec_all(i)
!!      end do
!!   
!!         rcounts(0) = pcount_vec_all(my_id)
!!         rdispl_all(0) = 0
!!      do i = 1, numprocs-1
!!         rcounts(i) = pcount_vec_all(my_id)
!!         rdispl_all(i) = rdispl_all(i-1) + rcounts(i-1)
!!      end do

      call MPI_ALLTOALLV(c_local_temp,scounts,sdispl_all,MPI_REAL8,   &
                         c_local_out,rcounts,rdispl_all,MPI_REAL8,MPI_COMM_WORLD,ierr)


         k = 0
      do j = istart, iend
         k = k + 1
         vector_out(j) = 0.0_idp
      do i = 0, numprocs-1
         vector_out(j) =  vector_out(j) + c_local_out( k+i*pcount_vec_all(my_id) )
      end do
      end do

!!      do i = istart,iend
!!         write(30+my_id,'(1x,i6,2x,es22.15)') i,vector_out(i)
!!      end do


      return
      end subroutine matvec



!===========================================================!
! This subroutine reads the input parameters and generate   !
! the mesh points of xi, eta, and other global variables.   !
! 'ier_read' returns the error code.                        !
!  ier_read = 0 for normal return without errors.           !
! Otherwise, it takes a non-zero integer.                   !
!===========================================================!
      subroutine datetime(message)
      use globalmod_constants, only  : idwrite
      implicit none
      character(LEN=20), intent(in) :: message
      character(LEN=8)              :: today
      character(LEN=10)             :: now


      call date_and_time (today, now)
! today: mm/dd/yyyy
      write(idwrite,'(1x,a,a2,a,a2,a,a4,a)') message, today(5:6),        &
                                     '/', today(7:8), '/', today(1:4),   &
                                     '  (/mm/dd/yyyy)'
! now: hhmmss.sss
      write(idwrite,'(1x,a,13x,a,a2,a,a2,a,a5)') 'Time',':: ',now(1:2), ':',    &
                                                           now(3:4), ':', now(5:10)
      write(idwrite,*)

      return
      end subroutine datetime

      MODULE globalmod_constants
          implicit none
          integer*4, parameter           ::  idread = 5, idwrite = 6
          character(LEN=50)              ::  machine_name
          integer*4                      ::  version,subversion
          integer, parameter             ::  idp = 8,master_id = 0
          integer*4                      ::  my_id,numprocs,COMM2D,COMM_ROW,COMM_COL, &
                                             my_id_row,my_id_col,mycoods_x,mycoods_y
          integer*4, dimension(1:2)      ::  coords_2d
          logical, dimension(1:2), parameter :: bdperiodic(1:2) = .FALSE.
          logical,                 parameter :: topology = .TRUE.


          integer*4                      ::  nsize,chunk,istart_col,iend_col,  &
                                             jstart_row,jend_row,noblock_1d,my_id_trans
          integer*4, dimension(1:2)      :: dimes
      END MODULE globalmod_constants




!=====================================================================!
! Matrix-vector products c = A * b.                                   !
!---------------------------------------------------------------------!
! (1) square dense martrix.                                           !
! (2) 2D block decomposition for matrix.                              !
! (3) numbers of virtual cores along two directions must be the same. !
! (4) total number of cores must be a square integer.                 ! 
! (5) matrix size is dividable by number of 1D cores.                 !
! (6) block decomposed for both vectors b and c.                      !
! (7) unit stride for local matrix-vector products.                   !
!---------------------------------------------------------------------!
! Xiaoxu Guan at LSU HPC                                              !
! April 26, 2016                                                      !
!=====================================================================!
      program mpi_matvec_v2
      use globalmod_constants
      implicit none
      include 'mpif.h'
      integer                 :: ierr,my_len
      integer*4               :: itemp,i,j,k,nmt,istart_min,iend_max,pcount,ptot,  &
     &                           ntemp,ic,jr
      real(KIND=idp)          :: time_s,time_e,what_time,temp,elapsed_time
      character(LEN=20)       :: message
      integer*4               :: istatus(MPI_STATUS_SIZE)
      integer                 :: argc            ! Number of command line arguments.
      integer                 :: iargc           ! External function returning argc.
      character(LEN=80)       :: arg             ! Container for command line arguments.
      integer*4               :: chunk_floor,chunk_ceili,rmn,pcount_mat,pcount_vec,km,kv,psize_mat,psize_vec
      real(KIND=idp), allocatable, dimension(:,:)  :: matrix
      real(KIND=idp), allocatable, dimension(:)    :: vector,&
                                                      vector_inp,vector_out,vector_inp_all,vector_out_all
      real(KIND=idp)                               :: mat



      call MPI_INIT( ierr )
      call MPI_COMM_SIZE( MPI_COMM_WORLD, numprocs, ierr )
      call MPI_COMM_RANK( MPI_COMM_WORLD, my_id, ierr )

      call MPI_GET_PROCESSOR_NAME( machine_name, my_len, ierr )
      call MPI_GET_VERSION( version, subversion, ierr )


      if(my_id == master_id ) then
         message = 'Job submitted on :: '
         call datetime(message)
      end if

      call MPI_BARRIER( MPI_COMM_WORLD,ierr )

      argc=iargc()
      if (argc /= 1) then ! number of input arguments must be 1.

          if (my_id == master_id) then
              write(idwrite,'(1x,a,i3)') 'Number of input arguments is ', argc
              write(idwrite,'(1x,a)')    'It must be 1.'
              write(idwrite,'(1x,a)')    'Quit!'
          end if
              call MPI_FINALIZE(ierr)
              stop
       else    ! passing the arguments.

          call getarg(1, arg)
          read(arg, '(i)') nsize   ! matrix size.

          if(my_id == master_id) then
             write(idwrite,'(1x,a,i10)')  'Matrix size: ', nsize
             write(idwrite,*)
          end if
       end if

      call MPI_BARRIER( MPI_COMM_WORLD,ierr )


! check parameters.
      temp = SQRT( dble(numprocs) )
      noblock_1d = IDNINT(temp)
      ntemp = noblock_1d * noblock_1d

      if(my_id==master_id) write(idwrite,'(1x,a,i4)')  'noblock_1d = ',noblock_1d

      if(ntemp /= numprocs) then
         write(idwrite,*) 'No. of cores is no a square number!'
         write(idwrite,*) 'numprocs = ', numprocs
         write(idwrite,*) 'noblock_1d = ', noblock_1d
         write(idwrite,*) 'Quit!'
         call MPI_FINALIZE(ierr)
         stop
      end if

      if( nsize < noblock_1d ) then
         write(idwrite,*)      &
         'Matrix size is too small or No. of cores too many!'
         write(idwrite,*) 'nsize = ', nsize
         write(idwrite,*) 'noblock_1d =', noblock_1d
         write(idwrite,*) 'Quit!'
         call MPI_FINALIZE(ierr)
         stop
      end if


! create a 2D Cartesian domain decomposition.
      dimes = noblock_1d
      call MPI_CART_CREATE(MPI_COMM_WORLD,2,dimes,bdperiodic,topology, &
                           COMM2D,ierr)
      call MPI_COMM_RANK(COMM2D,my_id,ierr)
      call MPI_CART_COORDS(COMM2D,my_id,2,coords_2d,ierr)

      mycoods_x = coords_2d(1)
      mycoods_y = coords_2d(2)



! domain decomposition (2D blocks).
      temp = dble(nsize) / dble(noblock_1d)
      chunk_floor = floor(temp) 
      chunk_ceili = ceiling(temp) 

      rmn = mod(nsize,noblock_1d) 

      if(rmn /= 0) then
         write(idwrite,*) &
               'Matrix size is not dividable by the number of 1d cores!'
         write(idwrite,*) 'nsize = ', nsize
         write(idwrite,*) 'number of 1d cores = ', noblock_1d
         write(idwrite,*) 'Quit!'
         call MPI_FINALIZE(ierr)
         stop
      end if


! get matrix indecies for each sub-matrix.
      chunk = chunk_floor
      istart_col = mycoods_x * chunk + 1
      iend_col   = istart_col + chunk - 1

      jstart_row = mycoods_y * chunk + 1
      jend_row   = jstart_row + chunk - 1

      write(idwrite,'(1x,8(i5,3x))')     &
      my_id, mycoods_x,mycoods_y,istart_col,iend_col,jstart_row,jend_row, chunk


! initialize sub-matrix and vector block.
! MPI tasks in the same row have the same vector blocks.
! note that each sub-matrix has the exactky same size and it's square.
      ALLOCATE( matrix(1:chunk,1:chunk) )
      ALLOCATE( vector(1:chunk) )


! vector elements only on the left-most cores.
      if(mycoods_x == 0) then
            my_id_trans = my_id * noblock_1d   ! transposed my_id.
            chunk = jend_row - jstart_row + 1
            jr = 0
         do j = jstart_row, jend_row
            jr = jr + 1
            vector(jr)   = COS( dble(j) ) 
         end do

!!         write(10+my_id,'(1x,es22.15)') vector

      end if

! send the block vector (column) to and recevie by its transposed id (row).
! from the tasks on the first column to the tasks on the first row.
      if(mycoods_x == 0 .and. my_id /= master_id) then
         call MPI_SEND(vector,chunk,MPI_REAL8,my_id_trans,0,MPI_COMM_WORLD,ierr)
      elseif( mycoods_y == 0 .and. my_id /= master_id) then
         my_id_trans = my_id / noblock_1d
         call MPI_RECV(vector,chunk,MPI_REAL8,my_id_trans,0,MPI_COMM_WORLD,istatus,ierr)
      end if



! matrix elements.
         jr = 0
      do j = jstart_row,jend_row
         jr = jr + 1
         ic = 0
      do i = istart_col,iend_col
         ic = ic + 1
         if(i <= j) then 
            matrix(jr,ic) = SIN( dble(i-j) )**(i+j)
         else
            matrix(jr,ic) = COS( dble(i-j) )**(i+j)
         end if
      end do
      end do

! split the communicator in terms of row and column.
      call MPI_COMM_SPLIT(MPI_COMM_WORLD,mycoods_y,mycoods_x,COMM_ROW,ierr)
      call MPI_COMM_RANK(COMM_ROW,my_id_row,ierr)

      call MPI_COMM_SPLIT(MPI_COMM_WORLD,mycoods_x,mycoods_y,COMM_COL,ierr)
      call MPI_COMM_RANK(COMM_COL,my_id_col,ierr)



      if(my_id==master_id) then
        write(idwrite,*)
        write(idwrite,'(1x,a)') 'Mapping in row and column blocks:'
      end if
      
      write(idwrite,'(1x,3(i4,2x))')  my_id, my_id_row, my_id_col

      call MPI_BARRIER( COMM2D,ierr )

! now roll over to each row.
      call MPI_BCAST(vector,chunk,MPI_REAL8,0,COMM_COL,ierr)



      ALLOCATE( vector_inp(1:chunk) )
      ALLOCATE( vector_out(1:chunk) )
      vector_inp = vector


!!      write(50+my_id,'(1x,i4,2x,i4,4x,i4,2x,i4)') LBOUND(vector_inp,1), UBOUND(vector_inp,1)


      if(my_id == master_id) then
         time_s = MPI_WTIME()
      end if


      do k = 1, 10000
         call matvec(matrix,vector_inp,vector_out)
         vector_inp = vector_out
! send the block vector (column) to and recevie by its transposed id (row).
! from the tasks on the first column to the tasks on the first row.
         if(mycoods_x == 0 .and. my_id /= master_id) then
            my_id_trans = my_id * noblock_1d   ! transposed my_id.
            call MPI_SEND(vector_inp,chunk,MPI_REAL8,my_id_trans,0,MPI_COMM_WORLD,ierr)
         elseif( mycoods_y == 0 .and. my_id /= master_id) then
            my_id_trans = my_id / noblock_1d
            call MPI_RECV(vector_inp,chunk,MPI_REAL8,my_id_trans,0,MPI_COMM_WORLD,istatus,ierr)
         end if

! now roll over to each row.
         call MPI_BCAST(vector_inp,chunk,MPI_REAL8,0,COMM_COL,ierr)
      end do

!!      if(mycoods_x == 0) then
!!         do i = 1, chunk
!!            write(30+my_id,'(1x,i6,2x,es22.15)') i, vector_out(i)
!!         end do
!!      end if


      if(my_id == master_id ) then
         elapsed_time = MPI_WTIME() - time_s
         write(idwrite,*)
         write(idwrite,'(1x,a,f12.6)') 'Elapsed_time (sec) = ', elapsed_time
      end if


      DEALLOCATE( matrix, vector )
      DEALLOCATE( vector_inp, vector_out )



      if(my_id == master_id ) then
         write(idwrite,*)
         write(idwrite,*)
         message = 'Job finished on  :: '
         call datetime(message)
      end if


! only master does all things.
!!      if(my_id==master_id) then
!!         ALLOCATE( vector_inp_all(1:nsize) )
!!         ALLOCATE( vector_out_all(1:nsize) )
!!         do i = 1, nsize
!!            vector_inp_all(i) =  COS( dble(i) )
!!         end do
!!
!!      do k = 1, 10000
!!         do i = 1, nsize
!!            temp = 0.0_idp
!!         do j = 1, nsize
!!            if(j <= i) then 
!!               mat = SIN( dble(j-i) )**(i+j)
!!            else
!!               mat = COS( dble(j-i) )**(i+j) 
!!            end if
!!            temp =  temp +  mat * vector_inp_all(j)
!!         end do
!!            vector_out_all(i) = temp
!!         end do
!!            vector_inp_all = vector_out_all
!!      end do
!!
!!!!         write(9,'(1x,i6,2x,es22.15)')  (i,vector_inp_all(i),i = 1,nsize)
!!      end if
! end of master block.


      call MPI_BARRIER( MPI_COMM_WORLD,ierr )

      call MPI_FINALIZE(ierr)

      end program mpi_matvec_v2



!===========================================================!
! MPI version of matrix-vector product. c = A * b.          ! 
!===========================================================!
      subroutine matvec(matrix,vector_inp,vector_out)
      use globalmod_constants, only  : idp,my_id,numprocs,nsize,chunk,istart_col,iend_col,  &
                                       mycoods_x,jstart_row,jend_row,COMM_ROW,COMM_COL

      implicit none
      include 'mpif.h'
      real(kind=idp), dimension(1:chunk,1:chunk), intent(in)   :: matrix
      real(kind=idp), dimension(1:chunk), intent(in)   :: vector_inp
      real(kind=idp), dimension(1:chunk), intent(out)  :: vector_out
      real(kind=idp), dimension(1:chunk)       :: c_local_temp
      integer*4        :: i,j,k,ierr
      real(kind=idp)   :: temp
     

         c_local_temp = 0.0_idp
      do i = 1, chunk
      do j = 1, chunk
         c_local_temp(j) = c_local_temp(j) + matrix(j,i) * vector_inp(i)
      end do
      end do

!!      if(my_id==0.or.my_id==3.or.my_id==6) then
!!      do i = 1, chunk
!!         write(20+my_id,'(1x,i6,2x,es22.15,2x,es22.15)') i, c_local_temp(i), vector_inp(i)
!!      end do
!!      end if


! reduce in all COMM_ROW.
      call MPI_REDUCE(c_local_temp,vector_out,chunk,MPI_REAL8,MPI_SUM,0,COMM_ROW,ierr)

!!      if(mycoods_x == 0) then
!!         do i = 1, chunk
!!            write(30+my_id,'(1x,i6,2x,es22.15)') i, vector_out(i)
!!         end do
!!      end if


      return
      end subroutine matvec



!===========================================================!
! This subroutine reads the input parameters and generate   !
! the mesh points of xi, eta, and other global variables.   !
! 'ier_read' returns the error code.                        !
!  ier_read = 0 for normal return without errors.           !
! Otherwise, it takes a non-zero integer.                   !
!===========================================================!
      subroutine datetime(message)
      use globalmod_constants, only  : idwrite
      implicit none
      character(LEN=20), intent(in) :: message
      character(LEN=8)              :: today
      character(LEN=10)             :: now


      call date_and_time (today, now)
! today: mm/dd/yyyy
      write(idwrite,'(1x,a,a2,a,a2,a,a4,a)') message, today(5:6),        &
                                     '/', today(7:8), '/', today(1:4),   &
                                     '  (/mm/dd/yyyy)'
! now: hhmmss.sss
      write(idwrite,'(1x,a,13x,a,a2,a,a2,a,a5)') 'Time',':: ',now(1:2), ':',    &
                                                           now(3:4), ':', now(5:10)
      write(idwrite,*)

      return
      end subroutine datetime

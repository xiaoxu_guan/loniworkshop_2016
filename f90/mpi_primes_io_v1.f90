      MODULE globalmod_constants
          implicit none
          integer*4, parameter           ::  idread = 5, idwrite = 6
          character(LEN=50)              ::  machine_name
          integer*4                      ::  version,subversion
          integer, parameter             ::  idp = 8,master_id = 0
          integer*4                      ::  my_id,numprocs
      END MODULE globalmod_constants



!=================================================================!
! Find all prime integers below N.                                !
!-----------------------------------------------------------------!
! (1) undefine the array for integer list <= N.                   !
! (2) only determine the first multiple of k in local region.     !
! (3) all the rest multiple of k move with the step of k.         !
! (4) delete all even integers, except 2.                         !
!-----------------------------------------------------------------!
! Xiaoxu Guan at LSU HPC                                          !
! April 18, 2016                                                  !
!=================================================================!
      program mpi_primes_io1
      use globalmod_constants
      implicit none
      include 'mpif.h'
      integer                 :: ierr,my_len
      integer*4               :: pmin,pmax,istart,iend,itemp,i,k,kmin,istart_min,iend_max,pcount,ptot,  &
     &                           pmax_org,lk,llk
      real(KIND=idp)          :: time_s,time_e,what_time,temp
      character(LEN=20)       :: message
      integer*4               :: istatus(MPI_STATUS_SIZE)
      integer                 :: argc            ! Number of command line arguments.
      integer                 :: iargc           ! External function returning argc.
      character(LEN=80)       :: arg             ! Container for command line arguments.
      integer*4               :: chunk_floor,chunk_ceili,chunk,rmn
!!      integer*4, allocatable, dimension(:)  :: idata     ! a list of !integers.
      logical, allocatable, dimension(:)    :: marked
      integer*4, allocatable, dimension(:)  :: marked_primes,primes,pcount_all,displ,data_out

! MPI I/O:
      character(LEN=10), parameter   :: primes_filename = 'primes.dat'
      integer*4                      :: filename,irequest,icount,ifs,psize,file_size,iextent_integer,ifilesize
      integer(KIND=MPI_OFFSET_KIND)  :: ioffset,ioffset_view





      call MPI_INIT( ierr )
      call MPI_COMM_SIZE( MPI_COMM_WORLD, numprocs, ierr )
      call MPI_COMM_RANK( MPI_COMM_WORLD, my_id, ierr )

      call MPI_GET_PROCESSOR_NAME( machine_name, my_len, ierr )
      call MPI_GET_VERSION( version, subversion, ierr )


      if(my_id == master_id ) then
         message = 'Job submitted on :: '
         call datetime(message)
         time_s = MPI_WTIME()
      end if

      call MPI_BARRIER( MPI_COMM_WORLD,ierr )

      argc=iargc()
      if (argc /= 2) then ! number of input arguments must be 2.

          if (my_id == master_id) then
              write(idwrite,'(1x,a,i3)') 'Number of input arguments is ', argc
              write(idwrite,'(1x,a)')    'It must be 2.'
              write(idwrite,'(1x,a)')    'Quit!'
          end if
              call MPI_FINALIZE(ierr)
              stop
       else    ! passing the arguments.

          call getarg(1, arg)
          read(arg, '(i)') pmin   ! left limit.
          call getarg(2, arg)
          read(arg, '(i)') pmax   ! right limit.

          if(my_id == master_id) then
             write(idwrite,'(1x,a,i10)')  'Lower limit: ', pmin
             write(idwrite,'(1x,a,i10)')  'Upper limit: ', pmax
             write(idwrite,*)
          end if
       end if

      call MPI_BARRIER( MPI_COMM_WORLD,ierr )

! domain decomposition.
      pmax_org = pmax

      if( mod(pmax,2) == 0 ) pmax = pmax - 1 
      lk = (pmax-1)/2    ! lk is the total number of all odd integers to search through.


      temp = dble(lk) / dble(numprocs)
      chunk_floor = floor(temp) 
      chunk_ceili = ceiling(temp) 

      rmn = mod(lk,numprocs) 

! istart and iend are all odd integers that need to be searched through.
      if( rmn > 0 .and. my_id <= rmn-1 ) then
          chunk  = chunk_ceili
          istart = my_id * chunk * 2 + 1 + 2
          iend   = istart + (chunk - 1) * 2
      elseif( rmn > 0 .and. my_id > rmn-1 ) then
          chunk  = chunk_floor
          itemp  = chunk_ceili * rmn 
          itemp  = itemp * 2 + 1
          istart = itemp + 2 + (my_id - rmn) * chunk * 2
          iend   = istart + (chunk - 1) * 2
      elseif(rmn == 0) then
          chunk = chunk_floor
          istart = my_id * chunk * 2 + 1 + 2
          iend   = istart + (chunk - 1) * 2
      end if


      ALLOCATE( marked(1:chunk) )
      marked  = .true.


      write(idwrite,'(1x,i3,2(a,i10),a,i10,2(a,i10))') my_id,      &
     & '  istart = ', istart,'  iend = ',   &
     & iend, '  size = ', chunk,'  step = 2'
      write(idwrite,*)


      call MPI_BARRIER( MPI_COMM_WORLD,ierr )


! doing the Sieve of Eratosthenes searching.
         iend_max   = min(iend,pmax)
         k = 3

      do while( k*k <= pmax )
         istart_min = max(istart,k*k)
! determine the first multiple of k.
         rmn = mod(istart_min,k)
         if(rmn /= 0) istart_min = istart_min - rmn + k  

      do i = istart_min, iend_max, k
!!         itemp = mod(idata(i),k) 
!         itemp = mod(i+2,k) 
!         if(itemp == 0) marked(i) = .false.

         if( mod(i,2) /= 0 ) then
             lk = (i - istart) / 2 + 1
             marked(lk) = .false.
          end if
      end do

! makred(i) = true means unmarked, while = false means marked.
! find out the smaller unmarked array element > k.

         kmin = pmax   ! avoid infinite loops.
      smallest_k:  do i = istart, iend, 2
         llk = (i - istart) / 2 + 1
         if( marked(llk) .and. i > k ) then 
             kmin = i
             EXIT smallest_k
         end if
      end do  smallest_k



      call MPI_ALLREDUCE(kmin,k,1,MPI_INTEGER,MPI_MIN,MPI_COMM_WORLD,ierr)

      end do


            pcount = 0
      do i = istart, iend, 2
         lk = (i - istart) / 2 + 1
         if( marked(lk) ) then
            pcount = pcount + 1
         end if
      end do

      if(my_id==master_id) then
         pcount = pcount + 1
         ALLOCATE( marked_primes(1:pcount) )
         marked_primes(1) = 2
         k = 1
      else
         ALLOCATE( marked_primes(1:pcount) )
         k = 0
      end if

      do i = istart, iend, 2
         lk = (i - istart) / 2 + 1
         if( marked(lk) ) then
            k = k + 1
            marked_primes(k) = i
         end if
      end do

      pcount = k

      call MPI_REDUCE(pcount,ptot,1,MPI_INTEGER,MPI_SUM,master_id,MPI_COMM_WORLD,ierr)

      if(my_id == master_id) then 
         time_e = MPI_WTIME()
         write(idwrite,'(1x,a,i10,a,i10)')     &
     &   'No of primes below ',pmax_org, ' is ', ptot
         write(idwrite,'(1x,a,f10.4)') 'Elapsed time (sec) =  ', time_e - time_s
         
         ALLOCATE( primes(1:ptot), pcount_all(0:numprocs-1), displ(0:numprocs-1) )
      end if


      if(my_id == master_id) then
            pcount_all(0) = pcount
            displ(0) = 0
         do i = 1, numprocs-1
            call MPI_RECV(k,1,MPI_INTEGER,i,0,MPI_COMM_WORLD,istatus,ierr)
            pcount_all(i) = k
            displ(i) = SUM( pcount_all(0:i-1) )
         end do
      else
         call MPI_SEND(pcount,1,MPI_INTEGER,master_id,0,MPI_COMM_WORLD,ierr)
      end if



      call MPI_BARRIER( MPI_COMM_WORLD,ierr )


! MPI I/O.
! prepare pmin,pmax,ptot, and primes.
      if(my_id == master_id) then
         ALLOCATE( data_out(1:pcount+3) )
         data_out(1) = pmin
         data_out(2) = pmax
         data_out(3) = ptot
         data_out(4:pcount+3) = marked_primes(1:pcount)
         psize = 3 + pcount
      else
         ALLOCATE( data_out(1:pcount) )
         data_out(:) = marked_primes(:)
         psize = pcount
      end if

! get ioffset for each MPI task.
      if(my_id /= master_id) ALLOCATE( pcount_all(0:numprocs-1) )

      call MPI_BCAST(pcount_all,numprocs,MPI_INTEGER,master_id,MPI_COMM_WORLD,ierr)

      if(my_id == master_id) then
         ioffset = 0
      else
         ioffset = 3 + SUM( pcount_all(0:my_id-1) )
      end if

      call MPI_FILE_OPEN(MPI_COMM_WORLD,primes_filename,    &
                         MPI_MODE_WRONLY+MPI_MODE_CREATE,   &
                         MPI_INFO_NULL,filename,ierr)


      ioffset_view = 0
      call MPI_FILE_SET_VIEW(filename,ioffset_view,MPI_INTEGER,MPI_INTEGER,  &
                             'native',MPI_INFO_NULL,ierr)
 
! each MPI task writes its own part to the same file in binary format.
      call MPI_FILE_WRITE_AT(filename,ioffset,data_out,psize,MPI_INTEGER,istatus,ierr)
 
! compute the file size.
      call MPI_TYPE_EXTENT(MPI_INTEGER,iextent_integer,ierr)


      if(my_id==master_id) then
         file_size = iextent_integer * (3 + ptot)
         write(idwrite,'(1x,a,i5,a)')    &
              'File size should be ', file_size, ' bytes.'

! measure the file size.
         call MPI_FILE_GET_SIZE(filename,ifilesize,ierr)
         write(idwrite,'(1x,a,i5,a)')   &
               'File size from MPI_FILE_GET_SIZE = ', &
               ifilesize, ' bytes'
      end if

      call MPI_BARRIER( MPI_COMM_WORLD,ierr )


      DEALLOCATE( marked, marked_primes, data_out, pcount_all )
      if(my_id==master_id) DEALLOCATE( primes, displ )


      if(my_id == master_id ) then
         write(idwrite,*)
         write(idwrite,*)
         message = 'Job finished on  :: '
         call datetime(message)
      end if


      call MPI_FINALIZE(ierr)

      end program mpi_primes_io1




!===========================================================!
! This subroutine reads the input parameters and generate   !
! the mesh points of xi, eta, and other global variables.   !
! 'ier_read' returns the error code.                        !
!  ier_read = 0 for normal return without errors.           !
! Otherwise, it takes a non-zero integer.                   !
!===========================================================!
      subroutine datetime(message)
      use globalmod_constants, only  : idwrite
      implicit none
      character(LEN=20), intent(in) :: message
      character(LEN=8)              :: today
      character(LEN=10)             :: now


      call date_and_time (today, now)
! today: mm/dd/yyyy
      write(idwrite,'(1x,a,a2,a,a2,a,a4,a)') message, today(5:6),        &
                                     '/', today(7:8), '/', today(1:4),   &
                                     '  (/mm/dd/yyyy)'
! now: hhmmss.sss
      write(idwrite,'(1x,a,13x,a,a2,a,a2,a,a5)') 'Time',':: ',now(1:2), ':',    &
                                                           now(3:4), ':', now(5:10)
      write(idwrite,*)

      return
      end subroutine datetime

      MODULE globalmod_constants
          implicit none
          integer*4, parameter           ::  idread = 5, idwrite = 6
          character(LEN=50)              ::  machine_name
          integer*4                      ::  version,subversion
          integer, parameter             ::  idp = 8,master_id = 0
          integer*4                      ::  my_id,numprocs
      END MODULE globalmod_constants



!=================================================================!
! Generate a dense square matrix and a vector.                    !
!-----------------------------------------------------------------!
! (1) using MPI_FILE_WRITE_AT to write matrix and vector elements !
!     to external files.                                          !
!-----------------------------------------------------------------!
! Xiaoxu Guan at LSU HPC                                          !
! April 18, 2016                                                  !
!=================================================================!
      program mpi_matvec_elements
      use globalmod_constants
      implicit none
      include 'mpif.h'
      integer                 :: ierr,my_len
      integer*4               :: nsize,istart,iend,itemp,i,j,k,nmt,istart_min,iend_max,pcount,ptot,  &
     &                           pmax_org,lk,llk
      real(KIND=idp)          :: time_s,time_e,what_time,temp
      character(LEN=20)       :: message
      integer*4               :: istatus(MPI_STATUS_SIZE)
      integer                 :: argc            ! Number of command line arguments.
      integer                 :: iargc           ! External function returning argc.
      character(LEN=80)       :: arg             ! Container for command line arguments.
      integer*4               :: chunk_floor,chunk_ceili,chunk,rmn,pcount_mat,pcount_vec,km,kv,psize_mat,psize_vec
      logical, allocatable, dimension(:)    :: marked
      integer*4, allocatable, dimension(:)  :: marked_primes,primes,pcount_all,pcount_mat_all,pcount_vec_all
      real(KIND=idp), allocatable, dimension(:,:)  :: matrix
      real(KIND=idp), allocatable, dimension(:)    :: matrix_data_out,vector,vector_data_out

! MPI I/O:
      character(LEN=19), parameter   ::                           &
                         matrix_filename = 'matrix_elements.dat', &
                         vector_filename = 'vector_elements.dat'

      integer*4                      :: filename_mat,filename_vec,irequest,icount,ifs,psize,file_size,iextent_real8,ifilesize
      integer(KIND=MPI_OFFSET_KIND)  :: ioffset_mat,ioffset_vec,ioffset_view





      call MPI_INIT( ierr )
      call MPI_COMM_SIZE( MPI_COMM_WORLD, numprocs, ierr )
      call MPI_COMM_RANK( MPI_COMM_WORLD, my_id, ierr )

      call MPI_GET_PROCESSOR_NAME( machine_name, my_len, ierr )
      call MPI_GET_VERSION( version, subversion, ierr )


      if(my_id == master_id ) then
         message = 'Job submitted on :: '
         call datetime(message)
         time_s = MPI_WTIME()
      end if

      call MPI_BARRIER( MPI_COMM_WORLD,ierr )

      argc=iargc()
      if (argc /= 1) then ! number of input arguments must be 1.

          if (my_id == master_id) then
              write(idwrite,'(1x,a,i3)') 'Number of input arguments is ', argc
              write(idwrite,'(1x,a)')    'It must be 1.'
              write(idwrite,'(1x,a)')    'Quit!'
          end if
              call MPI_FINALIZE(ierr)
              stop
       else    ! passing the arguments.

          call getarg(1, arg)
          read(arg, '(i)') nsize   ! matrix size.

          if(my_id == master_id) then
             write(idwrite,'(1x,a,i10)')  'Matrix size: ', nsize
             write(idwrite,*)
          end if
       end if

      call MPI_BARRIER( MPI_COMM_WORLD,ierr )

! domain decomposition (columnwise blocks).
      temp = dble(nsize) / dble(numprocs)
      chunk_floor = floor(temp) 
      chunk_ceili = ceiling(temp) 

      rmn = mod(nsize,numprocs) 

! note the global index istart starts from 1 for the first core.
      if( rmn > 0 .and. my_id <= rmn-1 ) then
          chunk  = chunk_ceili
          istart = my_id * chunk + 1
          iend   = istart + chunk - 1
      elseif( rmn > 0 .and. my_id > rmn-1 ) then
          chunk  = chunk_floor
          itemp  = chunk_ceili * rmn 
          istart = itemp + 1 + (my_id - rmn ) * chunk
          iend   = istart + chunk - 1
      elseif(rmn == 0) then
          chunk = chunk_floor
          istart = my_id * chunk + 1
          iend   = istart + chunk - 1
      end if



      ALLOCATE( matrix(1:nsize,istart:iend), vector(istart:iend)  )

! generate matrix and vector elements.
      do j = istart, iend   ! columm
         vector(j)   = COS( dble(j) )   ! vector elements.
      do i = 1, nsize       ! row
         if(i <= j) then 
         matrix(i,j) = SIN( dble(i-j) ) **(i+j)   ! matrix elements.
         else
         matrix(i,j) = COS( dble(i-j) ) **(i+j)   ! matrix elements.
         end if
      end do
      end do


! MPI I/O.
      nmt = nsize * chunk + 5
      ALLOCATE( matrix_data_out(1:nmt), vector_data_out(1:chunk+5) )

      matrix_data_out(1) = dble(numprocs)
      matrix_data_out(2) = dble(nsize)
      matrix_data_out(3) = dble(chunk)
      matrix_data_out(4) = dble(istart)
      matrix_data_out(5) = dble(iend)

      vector_data_out(1) = dble(numprocs)
      vector_data_out(2) = dble(nsize)
      vector_data_out(3) = dble(chunk)
      vector_data_out(4) = dble(istart)
      vector_data_out(5) = dble(iend)

         km = 5
         kv = 5
      do j = istart, iend
         kv = kv + 1
         vector_data_out(kv) = vector(j)
      do i = 1, nsize
         km = km + 1
         matrix_data_out(km) = matrix(i,j)
      end do
      end do

 
      ALLOCATE( pcount_mat_all(0:numprocs-1), pcount_vec_all(0:numprocs-1) )

      if(my_id == master_id) then
            pcount_mat_all(0) = chunk*nsize + 5
            psize_mat = pcount_mat_all(0)
            pcount_vec_all(0) = chunk + 5
            pcount_vec = pcount_vec_all(0)
            psize_vec = pcount_vec
         do i = 1, numprocs-1
            call MPI_RECV(k,1,MPI_INTEGER,i,0,MPI_COMM_WORLD,istatus,ierr)
            pcount_mat_all(i) = k
            call MPI_RECV(j,1,MPI_INTEGER,i,1,MPI_COMM_WORLD,istatus,ierr)
            pcount_vec_all(i) = j
         end do
      else
            pcount_mat = chunk*nsize + 5
            psize_mat = pcount_mat
            pcount_vec = chunk + 5
            psize_vec = pcount_vec
            call MPI_SEND(pcount_mat,1,MPI_INTEGER,master_id,0,MPI_COMM_WORLD,ierr)
            call MPI_SEND(pcount_vec,1,MPI_INTEGER,master_id,1,MPI_COMM_WORLD,ierr)
      end if

      call MPI_BCAST(pcount_mat_all,numprocs,MPI_INTEGER,master_id,MPI_COMM_WORLD,ierr)
      call MPI_BCAST(pcount_vec_all,numprocs,MPI_INTEGER,master_id,MPI_COMM_WORLD,ierr)



! get offset.
      if(my_id == master_id) then
         ioffset_mat = 0
         ioffset_vec = 0
      else
         ioffset_mat = SUM( pcount_mat_all(0:my_id-1) )
         ioffset_vec = SUM( pcount_vec_all(0:my_id-1) )
      end if

      call MPI_FILE_OPEN(MPI_COMM_WORLD,matrix_filename,    &
                         MPI_MODE_WRONLY+MPI_MODE_CREATE,   &
                         MPI_INFO_NULL,filename_mat,ierr)

      call MPI_FILE_OPEN(MPI_COMM_WORLD,vector_filename,    &
                         MPI_MODE_WRONLY+MPI_MODE_CREATE,   &
                         MPI_INFO_NULL,filename_vec,ierr)


      ioffset_view = 0
      call MPI_FILE_SET_VIEW(filename_mat,ioffset_view,MPI_REAL8,MPI_REAL8,  &
                             'native',MPI_INFO_NULL,ierr)
 
      call MPI_FILE_SET_VIEW(filename_vec,ioffset_view,MPI_REAL8,MPI_REAL8,  &
                             'native',MPI_INFO_NULL,ierr)

! each MPI task writes its own part to the same file in binary format.
      call MPI_FILE_WRITE_AT(filename_mat,ioffset_mat,matrix_data_out,psize_mat,MPI_REAL8,istatus,ierr)
      call MPI_FILE_WRITE_AT(filename_vec,ioffset_vec,vector_data_out,psize_vec,MPI_REAL8,istatus,ierr)
 
      call MPI_BARRIER( MPI_COMM_WORLD,ierr )

! compute the file size.
      call MPI_TYPE_EXTENT(MPI_REAL8,iextent_real8,ierr)


      if(my_id==master_id) then
! for matrix.
         file_size = iextent_real8 * (5*numprocs + nsize*nsize)
         write(idwrite,'(1x,a,i12,a)')    &
              'File size for matrix should be ', file_size, ' bytes.'

! measure the file size.
         call MPI_FILE_GET_SIZE(filename_mat,ifilesize,ierr)
         write(idwrite,'(1x,a,i12,a)')   &
               'File size for matrix from MPI_FILE_GET_SIZE = ', &
               ifilesize, ' bytes'

         write(idwrite,*)

! for vector.
         file_size = iextent_real8 * (5*numprocs + nsize)
         write(idwrite,'(1x,a,i12,a)')    &
              'File size for vector should be ', file_size, ' bytes.'

! measure the file size.
         call MPI_FILE_GET_SIZE(filename_vec,ifilesize,ierr)
         write(idwrite,'(1x,a,i12,a)')   &
               'File size for vector from MPI_FILE_GET_SIZE = ', &
               ifilesize, ' bytes'

      end if


! close the files.
      call MPI_FILE_CLOSE(filename_mat,ierr)
      call MPI_FILE_CLOSE(filename_vec,ierr)

      call MPI_BARRIER( MPI_COMM_WORLD,ierr )

      
      DEALLOCATE( matrix,vector, matrix_data_out, vector_data_out )
      DEALLOCATE( pcount_mat_all, pcount_vec_all )


      if(my_id == master_id ) then
         write(idwrite,*)
         write(idwrite,*)
         message = 'Job finished on  :: '
         call datetime(message)
      end if


      call MPI_FINALIZE(ierr)

      end program mpi_matvec_elements



!===========================================================!
! This subroutine reads the input parameters and generate   !
! the mesh points of xi, eta, and other global variables.   !
! 'ier_read' returns the error code.                        !
!  ier_read = 0 for normal return without errors.           !
! Otherwise, it takes a non-zero integer.                   !
!===========================================================!
      subroutine datetime(message)
      use globalmod_constants, only  : idwrite
      implicit none
      character(LEN=20), intent(in) :: message
      character(LEN=8)              :: today
      character(LEN=10)             :: now


      call date_and_time (today, now)
! today: mm/dd/yyyy
      write(idwrite,'(1x,a,a2,a,a2,a,a4,a)') message, today(5:6),        &
                                     '/', today(7:8), '/', today(1:4),   &
                                     '  (/mm/dd/yyyy)'
! now: hhmmss.sss
      write(idwrite,'(1x,a,13x,a,a2,a,a2,a,a5)') 'Time',':: ',now(1:2), ':',    &
                                                           now(3:4), ':', now(5:10)
      write(idwrite,*)

      return
      end subroutine datetime

      MODULE globalmod_constants
          implicit none
          integer*4, parameter           ::  idread = 5, idwrite = 6
          character(LEN=50)              ::  machine_name
          integer*4                      ::  version,subversion
          integer, parameter             ::  idp = 8,master_id = 0
          integer*4                      ::  my_id,numprocs
      END MODULE globalmod_constants



!=================================================================!
! Find all prime integers below N.                                !
!-----------------------------------------------------------------!
! (1) assume pmin = 2.                                            !
! (2) define the array for integer list <= N.                     !
! (3) search through all local integers for multiple of k.        !
!-----------------------------------------------------------------!
! Xiaoxu Guan at LSU HPC                                          !
! April 18, 2016                                                  !
!=================================================================!
      program mpi_primes_v0
      use globalmod_constants
      implicit none
      include 'mpif.h'
      integer                 :: ierr,my_len
      integer*4               :: pmin,pmax,istart,iend,itemp,i,k,kmin,istart_min,iend_max,pcount,ptot
      real(KIND=idp)          :: time_s,time_e,what_time,temp
      character(LEN=20)       :: message
      integer*4               :: istatus(MPI_STATUS_SIZE)
      integer                 :: argc            ! number of command line arguments.
      integer                 :: iargc           ! external function returning argc.
      character(LEN=80)       :: arg             ! container for command line arguments.
      integer*4               :: chunk_floor,chunk_ceili,chunk,rmn
      integer*4, allocatable, dimension(:)  :: idata     ! a list of integers.
      logical, allocatable, dimension(:)    :: marked




      call MPI_INIT( ierr )
      call MPI_COMM_SIZE( MPI_COMM_WORLD, numprocs, ierr )
      call MPI_COMM_RANK( MPI_COMM_WORLD, my_id, ierr )

      call MPI_GET_PROCESSOR_NAME( machine_name, my_len, ierr )
      call MPI_GET_VERSION( version, subversion, ierr )


      if(my_id == master_id ) then
         message = 'Job submitted on :: '
         call datetime(message)
         time_s = MPI_WTIME()
      end if

      call MPI_BARRIER( MPI_COMM_WORLD,ierr )

      argc=iargc()
      if (argc /= 2) then ! number of input arguments must be 2.

          if (my_id == master_id) then
              write(idwrite,'(1x,a,i3)') 'Number of input arguments is ', argc
              write(idwrite,'(1x,a)')    'It must be 2.'
              write(idwrite,'(1x,a)')    'Quit!'
          end if
              call MPI_FINALIZE(ierr)
              stop
       else    ! passing the arguments.

          call getarg(1, arg)
          read(arg, '(i)') pmin   ! left limit.
          call getarg(2, arg)
          read(arg, '(i)') pmax   ! right limit.

          if(my_id == master_id) then
             write(idwrite,'(1x,a,i10)')  'Lower limit: ', pmin
             write(idwrite,'(1x,a,i10)')  'Upper limit: ', pmax
             write(idwrite,*)
          end if
       end if

      call MPI_BARRIER( MPI_COMM_WORLD,ierr )

! domain decomposition.
      temp = dble(pmax-1) / dble(numprocs)
      chunk_floor = floor(temp) 
      chunk_ceili = ceiling(temp) 

      rmn = mod(pmax-1,numprocs) 

! note global indices istart and iend are the array indices starting from 0 for the first core.
! the indices i in array corresponds to integer i+2.
      if( rmn > 0 .and. my_id <= rmn-1 ) then
          chunk  = chunk_ceili
          istart = my_id * chunk
          iend   = istart + chunk - 1
      elseif( rmn > 0 .and. my_id > rmn-1 ) then
          chunk  = chunk_floor
          itemp  = chunk_ceili * rmn - 1
          istart = itemp + 1 + (my_id - rmn ) * chunk
          iend   = istart + chunk - 1
      elseif(rmn == 0) then
          chunk = chunk_floor
          istart = my_id * chunk
          iend   = istart + chunk - 1
      end if


      ALLOCATE( idata(istart:iend), marked(istart:iend) )
      marked  = .true.

! assign values.
      do i = istart, iend 
         idata(i) = i + 2
      end do

      write(idwrite,'(1x,i3,2(a,i10),a,i10,2(a,i10))') my_id,      &
     & '  array ind istart = ', istart,'  array ind iend = ',   &
     & iend, '  size = ', chunk,'  from ',istart+2, '  to ', iend+2
      write(idwrite,*)


      call MPI_BARRIER( MPI_COMM_WORLD,ierr )


! start the Sieve of Eratosthenes searching.
         k = 2

      do while( k*k <= pmax )
         istart_min = max(istart+2,k*k) - 2
         iend_max   = min(iend+2,pmax) - 2
      do i = istart_min, iend_max
         itemp = mod(idata(i),k) 
         if(itemp == 0) marked(i) = .false.
      end do

! makred(i) = true means unmarked, while = false means marked.
! find out the smaller unmarked array element > k.

         kmin = pmax   ! avoid infinite loops.
      do i = istart, iend
         if( marked(i) ) then 
             if( idata(i) > k ) then
                 kmin = idata(i)
                 EXIT
             end if
         end if
      end do

      call MPI_ALLREDUCE(kmin,k,1,MPI_INTEGER,MPI_MIN,MPI_COMM_WORLD,ierr)

      end do


            pcount = 0
      do i = istart, iend
         if( marked(i) ) then
            pcount = pcount + 1
         end if
      end do


      call MPI_REDUCE(pcount,ptot,1,MPI_INTEGER,MPI_SUM,master_id,MPI_COMM_WORLD,ierr)
      

      if(my_id == master_id) then 
         time_e = MPI_WTIME()
         write(idwrite,'(1x,a,i10,a,i10)')     &
     &   'No of primes below ',pmax, ' is ', ptot
          write(idwrite,'(1x,a,f10.4)') 'Elapsed time (sec) =  ', time_e - time_s
      end if



      DEALLOCATE( idata, marked )

      call MPI_BARRIER( MPI_COMM_WORLD,ierr )



      if(my_id == master_id ) then
         write(idwrite,*)
         write(idwrite,*)
         message = 'Job finished on  :: '
         call datetime(message)
      end if


      call MPI_FINALIZE(ierr)

      end program mpi_primes_v0



!===========================================================!
! This subroutine reads the input parameters and generate   !
! the mesh points of xi, eta, and other global variables.   !
! 'ier_read' returns the error code.                        !
!  ier_read = 0 for normal return without errors.           !
! Otherwise, it takes a non-zero integer.                   !
!===========================================================!
      subroutine datetime(message)
      use globalmod_constants, only  : idwrite
      implicit none
      character(LEN=20), intent(in) :: message
      character(LEN=8)              :: today
      character(LEN=10)             :: now


      call date_and_time (today, now)
! today: mm/dd/yyyy
      write(idwrite,'(1x,a,a2,a,a2,a,a4,a)') message, today(5:6),        &
                                     '/', today(7:8), '/', today(1:4),   &
                                     '  (/mm/dd/yyyy)'
! now: hhmmss.sss
      write(idwrite,'(1x,a,13x,a,a2,a,a2,a,a5)') 'Time',':: ',now(1:2), ':',    &
                                                           now(3:4), ':', now(5:10)
      write(idwrite,*)

      return
      end subroutine datetime

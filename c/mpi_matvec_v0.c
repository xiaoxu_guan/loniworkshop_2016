/*   mpi_matvec_v0.c   */


#include <mpi.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#define master_id 0
#define idread 5, idwrite 6, master_id 0
#define MAX(a,b) ((a) > (b) ? a : b)
#define MIN(a,b) ((a) < (b) ? a : b)


int    my_id,numprocs,my_len;
int    nsize,istart,iend,chunk;
int    version,subversion;
char   machine_name[MPI_MAX_PROCESSOR_NAME];
int    *rdispl_all,*rcounts,*sdispl_all,*scounts,*pcount_vec_all;
double *vector_inp,*vector_out,*matrix;


/* ==================================================================
  Matrix-vector products c = A * b.                                
---------------------------------------------------------------------
  (1) square dense matrix.                                         
  (2) columnwise block decomposion for matrix.                     
  (3) block decomposed for both vectors b and c.                   
  (4) MPI_ALLTOALLV for vector communication.                      
  (5) non-unit stride for local matrix-vector products.            
--------------------------------------------------------------------
  Xiaoxu Guan at LSU HPC                                           
  version 0                                                        
  May 04, 2016                                                     
================================================================== */


int main (int argc, char *argv[])

{ 
  double elapsed_time,temp;
  double *matrix_data_inp,*vector_data_inp,*vector;
  double time_s,time_e;
  int    itemp,chunk_floor,chunk_ceili,rmn,pmax_org,lk,km;
  int    i,ii,j,k,nmt;
  int    *pcount_mat_all;
  int    pcount_mat,pcount_vec,psize_mat,psize_vec,file_size,ierror;
  bool   *marked;

// MPI I/O:
  const char  matrix_filename[] = "matrix_elements.dat", vector_filename[] = "vector_elements.dat";
  MPI_Status  istatus;
  MPI_Offset  ioffset_mat,ioffset_vec,ioffset_view,ifilesize;
  MPI_File    filename_mat,filename_vec;
  MPI_Info    mpiinfo;
  MPI_Aint    iextent_double;

// master tests matrix-vector product.
  double *vec_temp,*mat_temp;


  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD,&numprocs);
  MPI_Comm_rank(MPI_COMM_WORLD,&my_id);
  
  MPI_Get_processor_name(machine_name,&my_len);
  MPI_Get_version(&version,&subversion);

  if(!my_id) { datetime("\nJob submitted on"); }

  MPI_Barrier(MPI_COMM_WORLD);

  if(argc != 2) {
     if (!my_id) { printf("Number of input arguments is %d\n", argc-1);
                   printf("It must be 1.\nQuit!"); }
     MPI_Finalize();
     exit(1);
  } 
  else {
     nsize = atoi(argv[1]);
  }


  if(!my_id) printf("Matrix size: %d\n", nsize);

  MPI_Barrier(MPI_COMM_WORLD);


// domain decomposition (columnwise blocks).
      temp = (double)(nsize) / (double)(numprocs);
      chunk_floor = (int) floor(temp);
      chunk_ceili = (int) ceil(temp);

      rmn =  nsize % numprocs;

// note the global index istart starts from 0 for the first core.
      if( rmn > 0 &&  my_id <= rmn-1 ) 
         {
          chunk  = chunk_ceili;
          istart = my_id * chunk;
          iend   = istart + chunk - 1; 
         }
      else if( rmn > 0 && my_id > rmn-1 ) 
         {
          chunk  = chunk_floor;
          itemp  = chunk_ceili * rmn;
          istart = itemp + (my_id - rmn) * chunk;
          iend   = istart + chunk - 1; 
         }
      else if(rmn == 0) 
         {
          chunk = chunk_floor;
          istart = my_id * chunk;
          iend   = istart + chunk - 1;
         }
      

   printf("%d %d %d %d\n", my_id, istart, iend, chunk);


// MPI Input.
   matrix = (double *) malloc (chunk*nsize*sizeof(double));
   vector = (double *) malloc (chunk*sizeof(double));
   pcount_mat_all = (int *) malloc (numprocs*sizeof(int));
   pcount_vec_all = (int *) malloc (numprocs*sizeof(int));


   if(!my_id) {
      pcount_mat_all[0] = chunk*nsize + 5;
      psize_mat = pcount_mat_all[0];
      pcount_vec_all[0] = chunk + 5;
      pcount_vec = pcount_vec_all[0];
      psize_vec = pcount_vec;
      for(i = 1; i < numprocs; i++) {
          MPI_Recv(&k,1,MPI_INT,i,0,MPI_COMM_WORLD,&istatus);
          pcount_mat_all[i] = k;
          MPI_Recv(&j,1,MPI_INT,i,1,MPI_COMM_WORLD,&istatus);
          pcount_vec_all[i] = j;
                                    }
      }
   else  {
      pcount_mat = chunk*nsize + 5;
      psize_mat = pcount_mat;
      pcount_vec = chunk + 5;
      psize_vec = pcount_vec;
      MPI_Send(&pcount_mat,1,MPI_INT,master_id,0,MPI_COMM_WORLD);
      MPI_Send(&pcount_vec,1,MPI_INT,master_id,1,MPI_COMM_WORLD);
      }

      MPI_Bcast(pcount_mat_all,numprocs,MPI_INT,master_id,MPI_COMM_WORLD);
      MPI_Bcast(pcount_vec_all,numprocs,MPI_INT,master_id,MPI_COMM_WORLD);


   nmt = nsize * chunk + 5;
   matrix_data_inp = (double *) malloc (nmt*sizeof(double));
   vector_data_inp = (double *) malloc ((chunk+5)*sizeof(double));


// get offset.
   if(!my_id) {
      ioffset_mat = 0;
      ioffset_vec = 0; 
   }
   else {
      ioffset_mat = 0;
      ioffset_vec = 0;
      for(i=0; i<my_id; i++) { 
          ioffset_mat = ioffset_mat + pcount_mat_all[i];
          ioffset_vec = ioffset_vec + pcount_vec_all[i];
          }
   }




// MPI I/O.
   ierror = MPI_File_open(MPI_COMM_WORLD,matrix_filename,MPI_MODE_RDONLY, \
                MPI_INFO_NULL,&filename_mat);
   if(ierror != MPI_SUCCESS) printf("Something wrong with MPI_File_open: matrix!");

   ierror = MPI_File_open(MPI_COMM_WORLD,vector_filename,MPI_MODE_RDONLY, \
                MPI_INFO_NULL,&filename_vec);
   if(ierror != MPI_SUCCESS) printf("Something wrong with MPI_File_open: vector!");


   ioffset_view = 0;
   MPI_File_set_view(filename_mat,ioffset_view,MPI_DOUBLE,MPI_DOUBLE,"native",MPI_INFO_NULL);
   if(ierror != MPI_SUCCESS) printf("Something wrong with MPI_File_set_view: matrix!");

   MPI_File_set_view(filename_vec,ioffset_view,MPI_DOUBLE,MPI_DOUBLE,"native",MPI_INFO_NULL);
   if(ierror != MPI_SUCCESS) printf("Something wrong with MPI_File_set_view: vector!");


// each MPI task writes its own part to the same file in binary format.
    MPI_File_read_at(filename_mat,ioffset_mat,matrix_data_inp,psize_mat,MPI_DOUBLE,&istatus);
    if(ierror != MPI_SUCCESS) printf("Something wrong with File_read_at: matrix!");

    MPI_File_read_at(filename_vec,ioffset_vec,vector_data_inp,psize_vec,MPI_DOUBLE,&istatus);
    if(ierror != MPI_SUCCESS) printf("Something wrong with File_red_at: vector!");


   MPI_Barrier(MPI_COMM_WORLD);

// close the files.
   MPI_File_close(&filename_mat);
   MPI_File_close(&filename_vec);


// check the matrix parameters.
   if( numprocs != (int) matrix_data_inp[0] )  {
       printf("my_id = %d\n", my_id);
       printf("numprocs != matrix_data_inp[0] !");
       printf("numprocs = %d\n", numprocs);
       printf("(int) matrix_data_inp[0] = %d\n", (int) matrix_data_inp[0]);
       printf("Quit!\n");
       MPI_Finalize(); }

   if( nsize != (int) matrix_data_inp[1] )  {
       printf("my_id = %d\n", my_id);
       printf("nsize != matrix_data_inp[1] !");
       printf("nsize = %d\n", nsize);
       printf("(int) matrix_data_inp[1] = %d\n", (int) matrix_data_inp[1]);
       printf("Quit!\n");
       MPI_Finalize(); }
    
   if( chunk != (int) matrix_data_inp[2] )  {
       printf("my_id = %d\n", my_id);
       printf("chunk != matrix_data_inp[2] !");
       printf("chunk = %d\n", chunk);
       printf("(int) matrix_data_inp[2] = %d\n", (int) matrix_data_inp[2]);
       printf("Quit!\n");
       MPI_Finalize(); }

   if( istart != (int) matrix_data_inp[3] )  {
       printf("my_id = %d\n", my_id);
       printf("istart != matrix_data_inp[3] !");
       printf("istart = %d\n", istart);
       printf("(int) matrix_data_inp[3] = %d\n", (int) matrix_data_inp[3]);
       printf("Quit!\n");
       MPI_Finalize(); }

   if( iend != (int) matrix_data_inp[4] )  {
       printf("my_id = %d\n", my_id);
       printf("iend != matrix_data_inp[4] !");
       printf("iend = %d\n", iend);
       printf("(int) matrix_data_inp[4] = %d\n", (int) matrix_data_inp[4]);
       printf("Quit!\n");
       MPI_Finalize(); }

// check the vector parameters.
   if( numprocs != (int) vector_data_inp[0] )  {
       printf("my_id = %d\n", my_id);
       printf("numprocs != vector_data_inp[0] !");
       printf("numprocs = %d\n", numprocs);
       printf("(int) vector_data_inp[0] = %d\n", (int) vector_data_inp[0]);
       printf("Quit!\n");
       MPI_Finalize(); }

   if( nsize != (int) vector_data_inp[1] )  {
       printf("my_id = %d\n", my_id);
       printf("nsize != vector_data_inp[1] !");
       printf("nsize = %d\n", nsize);
       printf("(int) vector_data_inp[1] = %d\n", (int) vector_data_inp[1]);
       printf("Quit!\n");
       MPI_Finalize(); }

   if( chunk != (int) vector_data_inp[2] )  {
       printf("my_id = %d\n", my_id);
       printf("chunk != vector_data_inp[2] !");
       printf("chunk = %d\n", chunk);
       printf("(int) vector_data_inp[2] = %d\n", (int) vector_data_inp[2]);
       printf("Quit!\n");
       MPI_Finalize(); }

   if( istart != (int) vector_data_inp[3] )  {
       printf("my_id = %d\n", my_id);
       printf("istart != vector_data_inp[3] !");
       printf("istart = %d\n", istart);
       printf("(int) vector_data_inp[3] = %d\n", (int) vector_data_inp[3]);
       printf("Quit!\n");
       MPI_Finalize(); }

   if( iend != (int) vector_data_inp[4] )  {
       printf("my_id = %d\n", my_id);
       printf("iend != vector_data_inp[4] !");
       printf("iend = %d\n", iend);
       printf("(int) vector_data_inp[4] = %d\n", (int) vector_data_inp[4]);
       printf("Quit!\n");
       MPI_Finalize(); }


// retrieve matrix and vector from the input data.
         km = 4;
   for( j = 0; j < chunk; j++)
    {
        vector[j] = vector_data_inp[j+5];
      for( i = 0; i < nsize; i++ ) 
       {
         matrix[km-5] = matrix_data_inp[++km];
       }
    }


  MPI_Barrier(MPI_COMM_WORLD);


// test matrix and vector elements.
       km = -1;
  for( j=istart; j<=iend; j++ ) // column 
   { if( abs( vector[j-istart] - cos((double) j) ) >= 1.e-10 ) printf("Something wrong with vector elements!\n");
     for( i=0; i<nsize; i++ ) 
     { if( i<=j && abs( matrix[++km] - pow( sin((double)(i-j)),i+j ) ) >= 1.e-10 ) 
       { printf("Something wrong with matrix elements i<=j!\n"); 
         printf("i = %d,  j = %d\n",i,j);
         printf("%f %f\n",matrix[km], pow( sin((double)(i-j)),i+j ));
       }
       else if ( i>j &&  abs( matrix[++km] - pow( cos((double)(i-j)),i+j ) ) >= 1.e-10 )
       { printf("Something wrong with matrix elements i>j!\n"); }
     }
   }
   

// only master does the following for testing.
// if(!my_id) {
//     vec_temp = (double *) malloc(nsize*sizeof(double));  
//     mat_temp = (double *) malloc(nsize*nsize*sizeof(double));  
//       km = -1;
//  for( j=0; j<nsize; j++ ) // column 
//   {   vec_temp[j] = cos((double) j); 
//     for( i=0; i<nsize; i++ ) 
//      {
//         if( i<=j ) { mat_temp[++km] = pow( sin((double)(i-j)),i+j ); }
//         else { mat_temp[++km] = pow( cos((double)(i-j)),i+j ); }
//      }
//
//   }
//
// matrix-vector products.
//   for(i = 0; i < nsize; i++) {
//       temp = 0.0;
//      for(j = 0; j < nsize; j++) {
//       temp = temp +  mat_temp[j*nsize+i] * vec_temp[j];
//        }
//     printf("%d  %22.15f\n",i,temp);
//   }
//
// }
// end of master test.




// define arrays for data communication in matrix-vector products.
    for(i=0; i<numprocs;i++) { pcount_vec_all[i] = pcount_vec_all[i]-5; } 

    rdispl_all = (int *) malloc (numprocs*sizeof(int) ); 
    rcounts    = (int *) malloc (numprocs*sizeof(int) ); 
    sdispl_all = (int *) malloc (numprocs*sizeof(int) ); 
    scounts    = (int *) malloc (numprocs*sizeof(int) ); 
     

         scounts[0] = pcount_vec_all[0];
         sdispl_all[0] = 0;

    for( i = 1; i <= numprocs-1; i++ )
     {
         sdispl_all[i] = sdispl_all[i-1] + scounts[i-1];
         scounts[i] = pcount_vec_all[i];
     }

         rcounts[0] = pcount_vec_all[my_id];
         rdispl_all[0] = 0;
      for( i = 1; i <= numprocs-1; i++ )
       {
         rcounts[i] = pcount_vec_all[my_id];
         rdispl_all[i] = rdispl_all[i-1] + rcounts[i-1];
       }

// all matrix and vector elemets are ready at this point.
   vector_inp = (double *) malloc (chunk*sizeof(double));
   vector_out = (double *) malloc (chunk*sizeof(double));

   for(i=0;i<chunk;i++) { vector_inp[i] = vector[i]; }

   if(!my_id) { time_s = MPI_Wtime(); }

   for(k=1; k<=10000; k++) 
    {  matvec(); 
       for(i=0; i<chunk; i++) { vector_inp[i] = vector_out[i]; }
    }


  if(!my_id) { time_e = MPI_Wtime(); printf("Elapsed time (sec) = %f\n", time_e-time_s); }
 
  free(matrix); free(vector); free(pcount_mat_all); free(pcount_vec_all);
  free(matrix_data_inp); free(vector_data_inp);
  free(rdispl_all); free(rcounts); free(sdispl_all); free(scounts);
  free(vector_inp); free(vector_out);

//  if(!my_id) { free(vec_temp); free(mat_temp); }


  MPI_Barrier(MPI_COMM_WORLD);

  if(!my_id) { datetime("\nJob finished on"); }

  MPI_Finalize();
  return 0;


}


//===================================================//
// MPI version of matrix-vector product. c = A *b.   //
//===================================================//
   matvec()
{
    int     i,j,k,km;
    double  c_local_temp[nsize],c_local_out[nsize*numprocs];


    for(i = 0; i < nsize; i++)
     {
            c_local_temp[i] = 0.0;
        for(j = 0; j < chunk; j++)
         {
            km = j * nsize + i;
            c_local_temp[i] = c_local_temp[i] + matrix[km] * vector_inp[j]; 
//            if(my_id==0) printf("mat x vec = %d %d %d %f\n" ,mk,j,i, matrix[mk] * vector_inp[j]);
          }
     }


//     if(!my_id) { for (i=0; i<nsize;i++) printf("%d,%12f\n",i,c_local_temp[i]);  }

     MPI_Alltoallv(c_local_temp,scounts,sdispl_all,MPI_DOUBLE,   \
                   c_local_out,rcounts,rdispl_all,MPI_DOUBLE,MPI_COMM_WORLD);


      for(j = 0; j < chunk; j++)
      {
         vector_out[j] = 0.0;
      for(i = 0; i < numprocs; i++)
        { vector_out[j] =  vector_out[j] + c_local_out[j+i*pcount_vec_all[my_id]]; }
      }


//     if(my_id==4) { for (i=0; i<chunk;i++) printf("%d,%22.15f\n",i,vector_out[i]);  }


}


//================================//
//    dat and time function       //
//================================//
    datetime(char message[])
{

   time_t t;
   t = time(NULL);
   printf("%s %s\n",message, ctime(&t));

}

/*   Sieve of Eratosthenes I/O v0   */


#include <mpi.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#define master_id 0
#define idread 5, idwrite 6, master_id 0
#define MAX(a,b) ((a) > (b) ? a : b)
#define MIN(a,b) ((a) < (b) ? a : b)


int   my_id,numprocs,my_len;
int   version,subversion;
char  machine_name[MPI_MAX_PROCESSOR_NAME];


/* ================================================================
  Find all prime integers below N.                               
-------------------------------------------------------------------
  (1) undefine the array for integer list <= N.                  
  (2) only determine the first multiple of k in local region.    
  (3) all the rest multiple of k move with the step of k.        
  (4) delete all even integers, except 2.                        
-------------------------------------------------------------------
  Xiaoxu Guan at LSU HPC                                         
  version 0                                                      
  May 01, 2016                                                   
================================================================ */


int main (int argc, char *argv[])

{ int    pmin,pmax;
  double temp;
  int    itemp,chunk_floor,chunk_ceili,rmn,pmax_org,lk,llk;
  int    istart,iend,chunk,i,j,ii,k,kmin,istart_min,iend_max;
  int    *primes,*pcount_all,*marked_primes,*data_out,*displ;
  int    pcount,ptot,psize,tsum,file_size,ierror;
  bool   *marked;

// MPI I/O:
  const char   primes_filename[] = "primes.dat";
  MPI_Status   istatus;
  MPI_Offset   ioffset,ioffset_view,ifilesize;
  MPI_File     filename;
  MPI_Info     mpiinfo;
  MPI_Aint     iextent_integer;
  MPI_Request  irequest;


  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD,&numprocs);
  MPI_Comm_rank(MPI_COMM_WORLD,&my_id);
  
  MPI_Get_processor_name(machine_name,&my_len);
  MPI_Get_version(&version,&subversion);

  if(!my_id) { datetime("\nJob submitted on"); }

  MPI_Barrier(MPI_COMM_WORLD);

  if(argc != 3) {
     if (!my_id) { printf("Number of input arguments is %d\n", argc-1);
                   printf("It must be 2.\nQuit!"); }
     MPI_Finalize();
     exit(1);
  } 
  else {
     pmin = atoi(argv[1]);
     pmax = atoi(argv[2]);
  }


  if(!my_id) printf("Lower limi: %d\nUpper limit: %d\n\n", pmin,pmax);


// domain decomposition.
      pmax_org = pmax;

      if( pmax % 2 == 0) pmax = pmax - 1;
      lk = (pmax-1)/2;  // ls is the total number of all odd integers to search through.


      temp = (double)(lk) / (double)(numprocs);
      chunk_floor = (int) floor(temp);
      chunk_ceili = (int) ceil(temp);

      rmn =  lk % numprocs;

// istart and iend are all odd integers that need to be serached through.
      if( rmn > 0 &&  my_id <= rmn-1 ) 
         {
          chunk  = chunk_ceili;
          istart = my_id * chunk *2  + 1 + 2;
          iend   = istart + (chunk - 1) * 2; 
         }
      else if( rmn > 0 && my_id > rmn-1 ) 
         {
          chunk  = chunk_floor;
          itemp  = chunk_ceili * rmn;
          itemp  = itemp * 2 + 1;
          istart = itemp + 2 + (my_id - rmn ) * chunk * 2;
          iend   = istart + (chunk - 1)*2; 
         }
      else if(rmn == 0) 
         {
          chunk = chunk_floor;
          istart = my_id * chunk * 2 + 1 + 2;
          iend   = istart + (chunk - 1) * 2;
         }
      
//   idata  = (int *) malloc (chunk*sizeof(int));
   marked = (bool *) malloc (chunk*sizeof(bool));

   for(i=0; i<chunk; i++) { marked[i] = true; }

// assign values.
// idata[0] = istart, [1] = istart+1, [2] = istart+2, ...
//   k = -1;
//   for(i=istart; i<=iend; i++) { idata[++k] = i + 2; }

   printf("%d array ind istart = %d array ind iend %d size = %d from %d to %d\n",  \
          my_id, istart,iend,chunk,istart,iend);


  MPI_Barrier(MPI_COMM_WORLD);


// start the Sieve of Eratosthenes searching.
      iend_max = MIN(iend,pmax);
      k = 3;

      do {
         istart_min = MAX(istart,k*k);
// determine the first multiple of k.
         rmn = istart_min % k;
         if(rmn != 0) istart_min = istart_min - rmn + k;

         for(i = istart_min; i <= iend_max; i+=k)
            {
//               itemp = idata[i-istart] % k;
//                  itemp = (i+2) % k;
//               if(itemp == 0) marked[i-istart] = false;
              if( i % 2 != 0 ) {               
                  lk = (i - istart) / 2;
                  marked[lk] = false; 
                 } 
            }

// makred[i] = true means unmarked, while = false means marked.
// find out the smaller unmarked array element > k.
         kmin = pmax;      // avoid infinite loops.
         for( i = istart; i <= iend; i+=2 )
            { 
               llk = (i - istart) / 2;
              if( marked[llk] && i > k ) {
//                  if( idata[i-istart] > k ) { kmin = idata[i-istart]; break; }
                  kmin = i; break; }
            }

          MPI_Allreduce(&kmin,&k,1,MPI_INT,MPI_MIN,MPI_COMM_WORLD);

      } while ( k*k <= pmax );


      pcount = 0;
      ii = -1;
   for(i=istart; i<=iend; i+=2)   // note both istart and iend must be odd.
    { 
      lk = (i - istart) / 2;
      if( marked[lk] ) ++pcount;
    }


   if(!my_id) {
      pcount = pcount + 1;
      marked_primes = (int *) malloc (pcount*sizeof(int));
      marked_primes[0] = 2;
      k = 0;
    }
   else {
      marked_primes = (int *) malloc (pcount*sizeof(int));
      k = -1;
    }

      
   for(i=istart;i<=iend; i+=2)
   {
      lk = (i - istart) / 2;
      if( marked[lk] ) { k = k+1; marked_primes[k] = i; }
   }


   MPI_Reduce(&pcount,&ptot,1,MPI_INT,MPI_SUM,master_id,MPI_COMM_WORLD);


   if(!my_id) {
      printf("No of primes below %d is %d\n\n",pmax_org, ptot);
      
      primes  = (int *) malloc (ptot*sizeof(int));
      pcount_all = (int *) malloc (numprocs*sizeof(int));

      displ = (int *) malloc (numprocs*sizeof(int));
    }


    if(!my_id) {
           pcount_all[0] = pcount;
           displ[0] = 0;
      for (i=1; i<=numprocs-1; i++) 
         {
           MPI_Recv(&k,1,MPI_INT,i,0,MPI_COMM_WORLD,&istatus);
           pcount_all[i] = k;
           displ[i] = 0;
           for(j=0;j<=i-1;j++) { displ[i] = displ[i] + pcount_all[j]; }
         }
     }
     else {
            MPI_Send(&pcount,1,MPI_INT,master_id,0,MPI_COMM_WORLD);
     }

     
  MPI_Barrier(MPI_COMM_WORLD);


// only master collects all primes and prints out.
  MPI_Gatherv(marked_primes,pcount,MPI_INT,primes,pcount_all,displ,MPI_INT,master_id,MPI_COMM_WORLD);


  if(!my_id) {    printf("prime list:\n");
     for (i=0; i<=ptot-1; i+=10) {   // each line prints 10 primes (max).
        for(ii=i; ii<=MIN(i+9,ptot-1); ii++)  { printf("%d  ", primes[ii]); }
            printf("\n");
               }
  }



  MPI_Barrier(MPI_COMM_WORLD);

  if(!my_id) { datetime("\nJob finished on"); }

  MPI_Finalize();
  return 0;


}




//================================//
//    dat and time function       //
//================================//

   datetime(char message[])
{

   time_t t;
   t = time(NULL);
   printf("%s %s\n",message, ctime(&t));

}

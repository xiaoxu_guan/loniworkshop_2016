/*   mpi_matvec_elements.c   */

#include <mpi.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#define master_id 0
#define idread 5, idwrite 6, master_id 0
#define MAX(a,b) ((a) > (b) ? a : b)
#define MIN(a,b) ((a) < (b) ? a : b)


int   my_id,numprocs,my_len;
int   version,subversion;
char  machine_name[MPI_MAX_PROCESSOR_NAME];


/* ===================================================================
  Generate a dense square matrix and a vector.                     
----------------------------------------------------------------------
  (1) using MPI_File_write_at to write matrix and vector elements  
      to external files.                                           
----------------------------------------------------------------------
  Xiaoxu Guan at LSU HPC                                           
  version 1                                                        
  May 04, 2016                                                     
=================================================================== */


int main (int argc, char *argv[])

{ int    nsize;
  double temp;
  double *matrix_data_out,*vector_data_out,*matrix,*vector;
  int    itemp,chunk_floor,chunk_ceili,rmn,pmax_org,lk,km,kv;
  int    istart,iend,chunk,i,ii,j,k,nmt;
  int    *pcount_mat_all,*pcount_vec_all;
  int    pcount_mat,pcount_vec,psize_mat,psize_vec,file_size,ierror;
  bool   *marked;

// MPI I/O:
  const char  matrix_filename[] = "matrix_elements.dat", vector_filename[] = "vector_elements.dat";
  MPI_Status  istatus;
  MPI_Offset  ioffset_mat,ioffset_vec,ioffset_view,ifilesize;
  MPI_File    filename_mat,filename_vec;
  MPI_Info    mpiinfo;
  MPI_Aint    iextent_double;


  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD,&numprocs);
  MPI_Comm_rank(MPI_COMM_WORLD,&my_id);
  
  MPI_Get_processor_name(machine_name,&my_len);
  MPI_Get_version(&version,&subversion);

  if(!my_id) { datetime("\nJob submitted on"); }

  MPI_Barrier(MPI_COMM_WORLD);

  if(argc != 2) {
     if (!my_id) { printf("Number of input arguments is %d\n", argc-1);
                   printf("It must be 1.\nQuit!"); }
     MPI_Finalize();
     exit(1);
  } 
  else {
     nsize = atoi(argv[1]);
  }


  if(!my_id) printf("Matrix size: %d\n", nsize);

  MPI_Barrier(MPI_COMM_WORLD);


// domain decomposition (columnwise blocks).
      temp = (double)(nsize) / (double)(numprocs);
      chunk_floor = (int) floor(temp);
      chunk_ceili = (int) ceil(temp);

      rmn =  nsize % numprocs;

// note the global index istart starts from 0 for the first core.
      if( rmn > 0 &&  my_id <= rmn-1 ) 
         {
          chunk  = chunk_ceili;
          istart = my_id * chunk;
          iend   = istart + chunk - 1; 
         }
      else if( rmn > 0 && my_id > rmn-1 ) 
         {
          chunk  = chunk_floor;
          itemp  = chunk_ceili * rmn;
          istart = itemp + (my_id - rmn) * chunk;
          iend   = istart + chunk - 1; 
         }
      else if(rmn == 0) 
         {
          chunk = chunk_floor;
          istart = my_id * chunk;
          iend   = istart + chunk - 1;
         }
      

   matrix = (double *) malloc (chunk*nsize*sizeof(double));
   vector = (double *) malloc (chunk*sizeof(double));


// generate matrix and vector elements.
        km = -1;
   for(j=istart; j<=iend; j++) { 
       vector[j-istart] = cos ((double) j);   // vector elements.
   for(i=0; i<nsize; i++) {
         km = km + 1;
       if( i <= j ) { matrix[km] = pow( sin((double)(i-j)), i+j );  
           //      if(!my_id) { printf("I am here id %d, %f",my_id, matrix[i,j-istart]); 
           //                   printf(" i = %d, j = %d, sin() %f\n", i,j, sin((double)(i-j)) ); 
                    }
       else { matrix[km] = pow( cos((double)(i-j)), i+j); }

//    if(!my_id) { printf("matrix[i,j] = %d %d %f\n", i,j-istart, matrix[km]); }

    }
    }


//   if(!my_id) printf("my_id = %d, istart = %d, iend = %d \n", my_id, istart,iend);


// MPI I/O.
   nmt = nsize * chunk + 5;

   matrix_data_out = (double *) malloc (nmt*sizeof(double));
   vector_data_out = (double *) malloc ((chunk+5)*sizeof(double));

   matrix_data_out[0] = (double)(numprocs);
   matrix_data_out[1] = (double)(nsize);
   matrix_data_out[2] = (double)(chunk);
   matrix_data_out[3] = (double)(istart);
   matrix_data_out[4] = (double)(iend);

   vector_data_out[0] = (double)(numprocs);
   vector_data_out[1] = (double)(nsize);
   vector_data_out[2] = (double)(chunk);
   vector_data_out[3] = (double)(istart);
   vector_data_out[4] = (double)(iend);

        km = -1;
        kv = 4;
   for( j=istart; j<=iend; j++)
    {
        vector_data_out[++kv] = vector[j-istart];
      for( i = 0; i < nsize; i++ ) 
       {
         matrix_data_out[km+5] = matrix[++km];
//          if(!my_id) printf("mat ele = %d %d %d %f\n", i,j-istart,km,matrix_data_out[km+5]);
       }
    }


   pcount_mat_all = (int *) malloc (numprocs*sizeof(int));
   pcount_vec_all = (int *) malloc (numprocs*sizeof(int));



   if(!my_id) {
      pcount_mat_all[0] = chunk*nsize + 5;
      psize_mat = pcount_mat_all[0];
      pcount_vec_all[0] = chunk + 5;
      pcount_vec = pcount_vec_all[0];
      psize_vec = pcount_vec;
      for(i = 1; i < numprocs; i++) {
          MPI_Recv(&k,1,MPI_INT,i,0,MPI_COMM_WORLD,&istatus);
          pcount_mat_all[i] = k;
          MPI_Recv(&j,1,MPI_INT,i,1,MPI_COMM_WORLD,&istatus);
          pcount_vec_all[i] = j;
                                    }
      }
   else  {
      pcount_mat = chunk*nsize + 5;
      psize_mat = pcount_mat;
      pcount_vec = chunk + 5;
      psize_vec = pcount_vec;
      MPI_Send(&pcount_mat,1,MPI_INT,master_id,0,MPI_COMM_WORLD);
      MPI_Send(&pcount_vec,1,MPI_INT,master_id,1,MPI_COMM_WORLD);
      }

      MPI_Bcast(pcount_mat_all,numprocs,MPI_INT,master_id,MPI_COMM_WORLD);
      MPI_Bcast(pcount_vec_all,numprocs,MPI_INT,master_id,MPI_COMM_WORLD);


// get offset.
   if(!my_id) {
      ioffset_mat = 0;
      ioffset_vec = 0; 
   }
   else {
      ioffset_mat = 0;
      ioffset_vec = 0;
      for(i=0; i<my_id; i++) { 
          ioffset_mat = ioffset_mat + pcount_mat_all[i];
          ioffset_vec = ioffset_vec + pcount_vec_all[i];
          }
   }



  MPI_Barrier(MPI_COMM_WORLD);



// MPI I/O.

   ierror = MPI_File_open(MPI_COMM_WORLD,matrix_filename,MPI_MODE_WRONLY | MPI_MODE_CREATE, \
                MPI_INFO_NULL,&filename_mat);
   if(ierror != MPI_SUCCESS) printf("Something wrong with MPI_File_open: matrix!");

   ierror = MPI_File_open(MPI_COMM_WORLD,vector_filename,MPI_MODE_WRONLY | MPI_MODE_CREATE, \
                MPI_INFO_NULL,&filename_vec);
   if(ierror != MPI_SUCCESS) printf("Something wrong with MPI_File_open: vector!");


   ioffset_view = 0;
   MPI_File_set_view(filename_mat,ioffset_view,MPI_DOUBLE,MPI_DOUBLE,"native",MPI_INFO_NULL);
   if(ierror != MPI_SUCCESS) printf("Something wrong with MPI_File_set_view: matrix!");

   MPI_File_set_view(filename_vec,ioffset_view,MPI_DOUBLE,MPI_DOUBLE,"native",MPI_INFO_NULL);
   if(ierror != MPI_SUCCESS) printf("Something wrong with MPI_File_set_view: vector!");

// each MPI task writes its own part to the same file in binary format.
    MPI_File_write_at(filename_mat,ioffset_mat,matrix_data_out,psize_mat,MPI_DOUBLE,&istatus);
    if(ierror != MPI_SUCCESS) printf("Something wrong with File_write_at: matrix!");

    MPI_File_write_at(filename_vec,ioffset_vec,vector_data_out,psize_vec,MPI_DOUBLE,&istatus);
    if(ierror != MPI_SUCCESS) printf("Something wrong with File_write_at: vector!");


// compute the file size.
    MPI_Type_extent(MPI_DOUBLE,&iextent_double);

    if(!my_id) {
// for matrix.
       file_size = iextent_double * (5*numprocs + nsize*nsize);
       printf("File size for matrix should be %d bytes.\n", file_size);

// measure the file size.
       MPI_File_get_size(filename_mat,&ifilesize);
       printf("File size for matrix from File_get_size = %d bytes.\n\n", (int) ifilesize);

// for vector.
       file_size = iextent_double * (5*numprocs + nsize);
       printf("File size for vector should be %d bytes.\n", file_size);

// measure the file size.
       MPI_File_get_size(filename_vec,&ifilesize);
       printf("File size for vector from File_get_size = %d bytes.\n", (int) ifilesize);
       
    }


// close the files.
  MPI_File_close(&filename_mat);
  MPI_File_close(&filename_vec);


  MPI_Barrier(MPI_COMM_WORLD);

  if(!my_id) { datetime("\nJob finished on"); }

  MPI_Finalize();
  return 0;


}




//================================//
//    dat and time function       //
//================================//

   datetime(char message[])
{

   time_t t;
   t = time(NULL);
   printf("%s %s\n",message, ctime(&t));

}

/*   Sieve of Eratosthenes v4   */


#include <mpi.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#define master_id 0
#define idread 5, idwrite 6, master_id 0
#define MAX(a,b) ((a) > (b) ? a : b)
#define MIN(a,b) ((a) < (b) ? a : b)


int   my_id,numprocs,my_len;
int   version,subversion;
char  machine_name[MPI_MAX_PROCESSOR_NAME];


/* =================================================================
  Find all prime integers below N.                               
--------------------------------------------------------------------
  (1) assume pmin = 2.                                           
  (2) undefine the array for integer list <= N.                  
  (3) only determine the first multiple of k in local region.    
  (4) all the rest multiple of k move with the step of k.        
  (5) delete all even integers, except 2.                        
  (6) Replace MPI_ALLREDUCE with MPI_REDUCE and MPI_BCAST.
--------------------------------------------------------------------
  Xiaoxu Guan at LSU HPC                                         
  version 4                                                      
  May 12, 2016                                                   
================================================================== */


int main (int argc, char *argv[])

{ int    pmin,pmax;
  double temp;
  double time_s,time_e;
  int    itemp,chunk_floor,chunk_ceili,rmn,pmax_org,lk,llk;
  int    istart,iend,chunk,i,ii,k,kmin,istart_min,iend_max;
//  int   *idata;
  int    pcount,ptot;
  bool   *marked;


  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD,&numprocs);
  MPI_Comm_rank(MPI_COMM_WORLD,&my_id);
  
  MPI_Get_processor_name(machine_name,&my_len);
  MPI_Get_version(&version,&subversion);

  if(!my_id) { datetime("\nJob submitted on"); time_s = MPI_Wtime(); }

  MPI_Barrier(MPI_COMM_WORLD);

  if(argc != 3) {
     if (!my_id) { printf("Number of input arguments is %d\n", argc-1);
                   printf("It must be 2.\nQuit!"); }
     MPI_Finalize();
     exit(1);
  } 
  else {
     pmin = atoi(argv[1]);
     pmax = atoi(argv[2]);
  }


  if(!my_id) printf("Lower limi: %d\nUpper limit: %d\n\n", pmin,pmax);


// domain decomposition.
      pmax_org = pmax;

      if( pmax % 2 == 0) pmax = pmax - 1;
      lk = (pmax-1)/2;  // ls is the total number of all odd integers to search through.


      temp = (double)(lk) / (double)(numprocs);
      chunk_floor = (int) floor(temp);
      chunk_ceili = (int) ceil(temp);

      rmn =  lk % numprocs;

// istart and iend are all odd integers that need to be serached through.
      if( rmn > 0 &&  my_id <= rmn-1 ) 
         {
          chunk  = chunk_ceili;
          istart = my_id * chunk *2  + 1 + 2;
          iend   = istart + (chunk - 1) * 2; 
         }
      else if( rmn > 0 && my_id > rmn-1 ) 
         {
          chunk  = chunk_floor;
          itemp  = chunk_ceili * rmn;
          itemp  = itemp * 2 + 1;
          istart = itemp + 2 + (my_id - rmn ) * chunk * 2;
          iend   = istart + (chunk - 1)*2; 
         }
      else if(rmn == 0) 
         {
          chunk = chunk_floor;
          istart = my_id * chunk * 2 + 1 + 2;
          iend   = istart + (chunk - 1) * 2;
         }
      
//   idata  = (int *) malloc (chunk*sizeof(int));
   marked = (bool *) malloc (chunk*sizeof(bool));

   for(i=0; i<chunk; i++) { marked[i] = true; }

// assign values.
// idata[0] = istart, [1] = istart+1, [2] = istart+2, ...
//   k = -1;
//   for(i=istart; i<=iend; i++) { idata[++k] = i + 2; }

   printf("%d array ind istart = %d array ind iend %d size = %d from %d to %d\n",  \
          my_id, istart,iend,chunk,istart,iend);


  MPI_Barrier(MPI_COMM_WORLD);


// start the Sieve of Eratosthenes searching.
      iend_max = MIN(iend,pmax);
      k = 3;

      do {
         istart_min = MAX(istart,k*k);
// determine the first multiple of k.
         rmn = istart_min % k;
         if(rmn != 0) istart_min = istart_min - rmn + k;

         for(i = istart_min; i <= iend_max; i+=k)
            {
//               itemp = idata[i-istart] % k;
//                  itemp = (i+2) % k;
//               if(itemp == 0) marked[i-istart] = false;
              if( i % 2 != 0 ) {               
                  lk = (i - istart) / 2;
                  marked[lk] = false; 
                 } 
            }

// makred[i] = true means unmarked, while = false means marked.
// find out the smaller unmarked array element > k.
         kmin = pmax;      // avoid infinite loops.
         for( i = istart; i <= iend; i+=2 )
            { 
               llk = (i - istart) / 2;
              if( marked[llk] && i > k ) {
//                  if( idata[i-istart] > k ) { kmin = idata[i-istart]; break; }
                  kmin = i; break; }
            }

//          MPI_Allreduce(&kmin,&k,1,MPI_INT,MPI_MIN,MPI_COMM_WORLD);
          MPI_Reduce(&kmin,&k,1,MPI_INT,MPI_MIN,master_id,MPI_COMM_WORLD);
          MPI_Bcast(&k,1,MPI_INT,master_id,MPI_COMM_WORLD);

      } while ( k*k <= pmax );


   pcount = 0;
   ii = -1;
   for(i=istart; i<=iend; i+=2)  // note both istart and iend must be odd.
     { if( marked[++ii] ) ++pcount; }


   MPI_Reduce(&pcount,&ptot,1,MPI_INT,MPI_SUM,master_id,MPI_COMM_WORLD);
   ptot = ptot + 1;


   if(!my_id) {
      printf("No of primes below %d is %d\n\n",pmax_org, ptot);
      time_e = MPI_Wtime();
      printf("Elapsed time (sec) = %f\n", time_e-time_s);
      datetime("Job finished on");
   }


  free(marked);

  MPI_Finalize();
  return 0;


}




//================================//
//    dat and time function       //
//================================//

   datetime(char message[])
{

   time_t t;
   t = time(NULL);
   printf("%s %s\n",message, ctime(&t));

}

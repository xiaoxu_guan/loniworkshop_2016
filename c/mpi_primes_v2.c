/*   Sieve of Eratosthenes v2   */


#include <mpi.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#define master_id 0
#define idread 5, idwrite 6, master_id 0
#define MAX(a,b) ((a) > (b) ? a : b)
#define MIN(a,b) ((a) < (b) ? a : b)


int   my_id,numprocs,my_len;
int   version,subversion;
char  machine_name[MPI_MAX_PROCESSOR_NAME];


/* =================================================================
  Find all prime integers below N.                               
--------------------------------------------------------------------
  (1) assume pmin = 2.                                           
  (2) undefine the array for integer list <= N.                  
  (3) only determine the first multiple of k in local region.    
  (4) all the rest multiple of k move with the step of k.        
--------------------------------------------------------------------
  Xiaoxu Guan at LSU HPC                                         
  version 2                                                      
  May 02, 2016                                                   
================================================================== */


int main (int argc, char *argv[])

{ int    pmin,pmax;
  double temp;
  double time_s,time_e;
  int    itemp,chunk_floor,chunk_ceili,rmn;
  int    istart,iend,chunk,i,ii,k,kmin,istart_min,iend_max;
//  int   *idata;
  int    pcount,ptot;
  bool   *marked;


  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD,&numprocs);
  MPI_Comm_rank(MPI_COMM_WORLD,&my_id);
  
  MPI_Get_processor_name(machine_name,&my_len);
  MPI_Get_version(&version,&subversion);

  if(!my_id) { datetime("\nJob submitted on");  time_s = MPI_Wtime(); }

  MPI_Barrier(MPI_COMM_WORLD);

  if(argc != 3) {
     if (!my_id) { printf("Number of input arguments is %d\n", argc-1);
                   printf("It must be 2.\nQuit!"); }
     MPI_Finalize();
     exit(1);
  } 
  else {
     pmin = atoi(argv[1]);
     pmax = atoi(argv[2]);
  }


  if(!my_id) printf("Lower limi: %d\nUpper limit: %d\n\n", pmin,pmax);


// domain decomposition.
      temp = (double)(pmax-1) / (double)(numprocs);
      chunk_floor = (int) floor(temp);
      chunk_ceili = (int) ceil(temp);

      rmn =  (pmax-1) % numprocs ;

// note global indices istart and iend are the array indices starting from 0 for the first core.
// the indices i in array corresponds to integer i+2.
      if( rmn > 0 &&  my_id <= rmn-1 ) 
         {
          chunk  = chunk_ceili;
          istart = my_id * chunk;
          iend   = istart + chunk - 1; 
         }
      else if( rmn > 0 && my_id > rmn-1 ) 
         {
          chunk  = chunk_floor;
          itemp  = chunk_ceili * rmn - 1;
          istart = itemp + 1 + (my_id - rmn ) * chunk;
          iend   = istart + chunk - 1; 
         }
      else if(rmn == 0) 
         {
          chunk = chunk_floor;
          istart = my_id * chunk;
          iend   = istart + chunk - 1;
         }
      
//   idata  = (int *) malloc (chunk*sizeof(int));
   marked = (bool *) malloc (chunk*sizeof(bool));

   for(i=0; i<chunk; i++) { marked[i] = true; }

// assign values.
// idata[0] = istart, [1] = istart+1, [2] = istart+2, ...
//   k = -1;
//   for(i=istart; i<=iend; i++) { idata[++k] = i + 2; }

   printf("%d array ind istart = %d array ind iend %d size = %d from %d to %d\n",  \
          my_id, istart,iend,chunk,istart+2,iend+2);


  MPI_Barrier(MPI_COMM_WORLD);


// start the Sieve of Eratosthenes searching.
      iend_max = MIN(iend+2,pmax);
      k = 2;

      do {
         istart_min = MAX(istart+2,k*k);
//         istart_min = MAX(istart+2,k*k) - 2;
//         iend_max   = MIN(iend+2,pmax) - 2;
         rmn = istart_min % k;
         if(rmn != 0) istart_min = istart_min - rmn + k;
         for(i = istart_min; i <= iend_max; i+=k)
            {
//               itemp = idata[i-istart] % k;
//                  itemp = (i+2) % k;
//               if(itemp == 0) marked[i-istart] = false;
              marked[i-istart-2] = false;                
            }

// makred[i] = true means unmarked, while = false means marked.
// find out the smaller unmarked array element > k.
         kmin = pmax;      // avoid infinite loops.
         for( i = istart; i <= iend; i++)
            { 
              if( marked[i-istart-2] && i > k ) {
//                  if( idata[i-istart] > k ) { kmin = idata[i-istart]; break; }
                  kmin = i; break; }
            }

          MPI_Allreduce(&kmin,&k,1,MPI_INT,MPI_MIN,MPI_COMM_WORLD);

      } while ( k*k <= pmax );


   pcount = 0;
   ii = -1;
   for(i=istart; i<=iend; i++) 
     { if( marked[++ii] ) ++pcount; }


   MPI_Reduce(&pcount,&ptot,1,MPI_INT,MPI_SUM,master_id,MPI_COMM_WORLD);


   if(!my_id) {
      printf("No of primes below %d is %d\n\n",pmax, ptot);
      time_e = MPI_Wtime();
      printf("Elapsed time (sec) = %f\n", time_e-time_s);
      datetime("Job finished on");
   }


  free(marked);

  MPI_Finalize();
  return 0;


}




//================================//
//    dat and time function       //
//================================//

   datetime(char message[])
{

   time_t t;
   t = time(NULL);
   printf("%s %s\n",message, ctime(&t));

}

/*   mpi_matvec_v2.c   */


#include <mpi.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#define master_id 0
#define idread 5, idwrite 6, master_id 0
#define MAX(a,b) ((a) > (b) ? a : b)
#define MIN(a,b) ((a) < (b) ? a : b)
#define kmax 10000


int       my_id,numprocs,my_len;
int       nsize,istart,iend,chunk;
int       version,subversion;
char      machine_name[MPI_MAX_PROCESSOR_NAME];
int       *rdispl_all,*rcounts,*sdispl_all,*scounts,*pcount_vec_all;
int       coords_2d[1],mycoods_x,mycoods_y,my_id_row,my_id_col;
int       my_id_trans,noblock_1d,istart_col,iend_col,jstart_row,jend_row;
int       bdperiodic[1],dimes[1];
MPI_Comm  COMM2D,COMM_ROW,COMM_COL;



/* ====================================================================
  Matrix-vector products c = A * b.                                   
-----------------------------------------------------------------------
  (1) square dense matrix.                                            
  (2) 2D block decomposition for matrix.                              
  (3) numbers of virtual cores along two directions must be the same. 
  (4) total number of cores must be a square integer.                  
  (5) matrix size is dividable by number of 1D cores.                
  (6) block decomposed for both vectors b and c.                      
  (7) unit stride for local matrix-vector products.                   
  (8) matrix is stored in column-wise.
-----------------------------------------------------------------------
  Xiaoxu Guan at LSU HPC                                              
  version 2_global                                                          
  May 26, 2016                                                        
===================================================================== */


int main (int argc, char *argv[])

{ 
  double elapsed_time,temp;
  double *matrix_data_inp,*vector_data_inp,*vector;
  double time_s,time_e;
  int    itemp,chunk_floor,chunk_ceili,rmn,pmax_org,lk,km;
  int    i,ii,j,k,jr,ntemp;
  int    *idata;
  int    *pcount_mat_all;
  int    pcount_mat,pcount_vec,psize_mat,psize_vec,file_size,ierror;
  double *matrix,*vector_inp,*vector_out;

MPI_Status  istatus;

void  datetime(char message[]);
void  matvec(double *matrix, double *vector_inp, double *vector_out);


//master test matrix-vector product.
double *vec_temp,*mat_temp;
const int topology = 1;

  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD,&numprocs);
  MPI_Comm_rank(MPI_COMM_WORLD,&my_id);
  
  MPI_Get_processor_name(machine_name,&my_len);
  MPI_Get_version(&version,&subversion);

 
  if(!my_id) { datetime("\nJob submitted on"); }

 
  MPI_Barrier(MPI_COMM_WORLD);

  if(argc != 2) {
     if (!my_id) { printf("Number of input arguments is %d\n", argc-1);
                   printf("It must be 1.\nQuit!"); }
     MPI_Finalize();
     exit(1);
  } 
  else {
     nsize = atoi(argv[1]);
  }


  if(!my_id) printf("Matrix size: %d\n", nsize);

  MPI_Barrier(MPI_COMM_WORLD);


// check paramters.
     temp = sqrt( (double) (numprocs) );
     noblock_1d = (int) (temp);
     ntemp = noblock_1d * noblock_1d;

     if(!my_id) { printf("noblock_1d = %d\n", noblock_1d); }

     if(ntemp != numprocs) {
        printf("No. of cores is no a square number!\n");
        printf("numprocs = %d\n", numprocs);
        printf("noblock_1d = %d\n", noblock_1d);
        printf("Quit!\n");
        MPI_Barrier(MPI_COMM_WORLD);
        MPI_Finalize();
      }


     if( nsize < noblock_1d ) {
        printf("Matrix size is too small or No. of cores too many!\n");
        printf("nsize = %d\n", nsize);
        printf("noblock_1d = %d\n", noblock_1d);
        printf("Quit!\n");
        MPI_Barrier(MPI_COMM_WORLD);
        MPI_Finalize();
      }


// create a 2D Cartesian domain decomposition.
      bdperiodic[0] = 0;
      bdperiodic[1] = 0;
      dimes[0] = noblock_1d;
      dimes[1] = noblock_1d;
      MPI_Cart_create(MPI_COMM_WORLD,2,dimes,bdperiodic,topology,&COMM2D);
      MPI_Comm_rank(COMM2D,&my_id);
      MPI_Cart_coords(COMM2D,my_id,2,coords_2d);

      mycoods_x = coords_2d[0];
      mycoods_y = coords_2d[1];


// domain decomposition (2D blocks).
      temp = (double)(nsize) / (double)(noblock_1d);
      chunk_floor = (int) floor(temp);
      chunk_ceili = (int) ceil(temp);

      rmn =  nsize % noblock_1d;

      if(rmn != 0 ) {
         printf("Matrix size is not dividable by the number of 1d cores!\n");
         printf("nsize = %d\n", nsize);
         printf("number of 1d cores = %d\n", noblock_1d);
         printf("rmn = %d\n",rmn);
         printf("Quit!\n");
         MPI_Barrier(MPI_COMM_WORLD);
         MPI_Finalize();
       }


// get matrix indecies for each sub-matrix.
      chunk = chunk_floor;
      istart_col = mycoods_x * chunk;
      iend_col   = istart_col + chunk - 1;

      jstart_row = mycoods_y * chunk;
      jend_row   = jstart_row + chunk - 1;

      printf("%d %d %d %d %d %d %d %d\n",my_id,mycoods_x,mycoods_y,istart_col,iend_col,jstart_row,jend_row,chunk);

// initialize sub-matrix and vector block.
// MPI tasks in the same row have the same vector blocks.
// note that each sub-matrix has the exactky same size and it's square.
      matrix = (double *) malloc (chunk*chunk*sizeof(double));
      vector = (double *) malloc (chunk*sizeof(double)) ;


// vector elements only on the left-most cores.
      if(mycoods_x == 0)
      {
         my_id_trans = my_id * noblock_1d;   // transposed my_id.
             jr = -1;
         for(j=jstart_row; j<=jend_row; j++) { vector[++jr] = cos( (double)(j+1) ); } 
// initial vector elements were shifted by one j --> j+1 to be consistent with Fortran code.
      }


// send the block vector (column) to and recevie by its transposed id (row).
// from the tasks on the first column to the tasks on the first row.
      if(mycoods_x == 0 && my_id != master_id) 
        { MPI_Send(vector,chunk,MPI_DOUBLE,my_id_trans,0,MPI_COMM_WORLD); }
      else if( mycoods_y == 0 && my_id != master_id)
        { my_id_trans = my_id / noblock_1d;
          MPI_Recv(vector,chunk,MPI_DOUBLE,my_id_trans,0,MPI_COMM_WORLD,&istatus);
        }


// matrix elements. each sub-matrix element is stored in column-wise. 
          km = -1;
      for(i = istart_col; i <= iend_col; i++)
      {
      for(j = jstart_row; j <= jend_row; j++)
        {
         if(i <= j) { matrix[++km] = pow( sin((double)(i-j)),(i+1+j+1) ); }
         else { matrix[++km] = pow( cos((double)(i-j)),(i+1+j+1) ); }
// matrix elements were shifted by one i --> i+1 and j --> j+1 to be consistent with Fortran code.
        }
      }


// split the communicator in terms of row and column.
      MPI_Comm_split(MPI_COMM_WORLD,mycoods_y,mycoods_x,&COMM_ROW);
      MPI_Comm_rank(COMM_ROW,&my_id_row);

      MPI_Comm_split(MPI_COMM_WORLD,mycoods_x,mycoods_y,&COMM_COL);
      MPI_Comm_rank(COMM_COL,&my_id_col);


      if(!my_id) { printf("\nMapping in row and column blocks:\n"); }

      printf("%d %d %d\n", my_id, my_id_row, my_id_col);

      MPI_Barrier( COMM2D );


// now roll over to each row.
      MPI_Bcast(vector,chunk,MPI_DOUBLE,0,COMM_COL);


      vector_inp = (double *) malloc (chunk*sizeof(double));
      vector_out = (double *) malloc (chunk*sizeof(double));

      for(i=0; i<chunk; i++) { vector_inp[i] = vector[i]; }

      if(!my_id) { time_s = MPI_Wtime(); }


      for(k=1; k<=kmax; k++) {
          matvec(matrix,vector_inp,vector_out);
          for(i=0; i<chunk; i++) { vector_inp[i] = vector_out[i]; }


//   if(k==kmax)
//   {
//      if(my_id == 1) { for(i=0; i<chunk; i++) { printf("%2d  %22.15f\n", i, vector_out[i]); } }
//   }


// for next matrix-vector product.
// send the block vector (column) to and recevie by its transposed id (row).
// from the tasks on the first column to the tasks on the first row.
         if(mycoods_x == 0 && my_id != master_id)
          {
            my_id_trans = my_id * noblock_1d;   // transposed my_id.
            MPI_Send(vector_inp,chunk,MPI_DOUBLE,my_id_trans,0,MPI_COMM_WORLD);
          }
         else if( mycoods_y == 0 && my_id != master_id)
          {
            my_id_trans = my_id / noblock_1d;
            MPI_Recv(vector_inp,chunk,MPI_DOUBLE,my_id_trans,0,MPI_COMM_WORLD,&istatus);
          }

// now roll over to each row.
            MPI_Bcast(vector_inp,chunk,MPI_DOUBLE,0,COMM_COL);

      }


  if(!my_id) { time_e = MPI_Wtime(); printf("Elapsed time (sec) = %12.6f\n", time_e-time_s); }


  free(matrix); free(vector); free(vector_inp); free(vector_out);


  MPI_Barrier(MPI_COMM_WORLD);

  if(!my_id) { datetime("\nJob finished on"); }

  MPI_Finalize();
  return 0;


}


//===================================================//
// MPI version of matrix-vector product. c = A * b.  //
//===================================================//
void  matvec(double *matrix, double *vector_inp, double *vector_out)
{
    int     i,j,k,km;
    double  c_local_temp[chunk];


    for(i=0; i<chunk; i++) { c_local_temp[i] = 0.0; }

      km = -1;
    for(i = 0; i < chunk; i++)
     {
      for(j = 0; j < chunk; j++)
         {  c_local_temp[j] = c_local_temp[j] + matrix[++km] * vector_inp[i]; 
         }
     }

//     if(!my_id) {
//       for(i=0;i<chunk;i++) printf("%2d  %22.15f %22.15f \n", i,c_local_temp[i], vector_inp[i]);
//     }


// reduce in all COMM_ROW.
    MPI_Reduce(c_local_temp,vector_out,chunk,MPI_DOUBLE,MPI_SUM,0,COMM_ROW);

}



//================================//
//    dat and time function       //
//================================//
void  datetime(char message[])
{

   time_t t;
   t = time(NULL);
   printf("%s %s\n",message, ctime(&t));

}
